package webbbsc;

class Rules {

	public enum DoubleType
	{
		DAny("Double Any 2 Cards"),
		DOnly_9_10_11("Double 9, 10, 11 Only"),
		DOnly_10_11("Double 10, 11 Only");
		
		//Better human readable than just name(), to represent in GUI or somewhere
		private final String stringRep;
		
		DoubleType(String str){
			stringRep = str;
		}
		
		@Override
		public String toString(){
			return stringRep;
		}
		
		public static DoubleType fromString(String str){
			for(DoubleType d: DoubleType.values()){
				if(d.toString().equals(str))
					return d;
			}
			throw new IllegalArgumentException(str + " does not match any DoubleType");
		}
	};

	public enum Surrender
	{
		SNo("No Surrender"),
		SLate("Late Surrender"),
		SEarly("Early Surrender");

		private final String stringRep;
		
		Surrender(String str){
			stringRep = str;
		}
		
		@Override
		public String toString(){
			return stringRep;
		}

		public static Surrender fromString(String str){
			for(Surrender s: Surrender.values()){
				if(s.toString().equals(str))
					return s;
			}
			throw new IllegalArgumentException(str + " does not match any SurrenderType");
		}
	};
	
	public void setMaxSplits(int maxSpl){
		if(maxSpl < MinMaxSplits)
			maxSplits = MinMaxSplits;
		else if(maxSpl > MaxMaxSplits)
			maxSplits = MaxMaxSplits;
		else
			maxSplits = maxSpl;
	}

	public DoubleType	dbl;
	public int			maxSplits;
	public Surrender	surr;
	public boolean      dealerHitsS17;
	public boolean      dealerPeeks;
	public boolean      dblAfterSplitAllowed;
	public boolean      acesResplitAllowed;
	
	public static final int MinMaxSplits = 0;
	public static final int MaxMaxSplits = 5;
	
	{
        dbl = DoubleType.DAny;
        surr = Surrender.SNo;
        maxSplits = 3;
        dealerHitsS17 = false;
        dealerPeeks = false;
        dblAfterSplitAllowed = true;
        acesResplitAllowed = false;
	}
}
