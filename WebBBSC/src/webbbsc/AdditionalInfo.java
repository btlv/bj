package webbbsc;

import java.io.File;

import com.vaadin.ui.Panel;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window;
import com.vaadin.terminal.FileResource;


@SuppressWarnings("serial")
class AdditionalInfo extends Panel {
	public void init(){
		setCaption("Additional Info");

		Button downloadButton = new Button("Download Desktop Version", new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				FileResource file = new FileResource(new File("./SetupWinBBSC.exe"), getApplication());
				Window mainWindow = getApplication().getMainWindow();
				if(downloadWindow.getParent() == null)
					mainWindow.addWindow(downloadWindow);
				downloadWindow.open(file);
			}
		});
		downloadButton.setWidth("100%");
		addComponent(downloadButton);

		//Label contactLabel = new Label("By Victor Knyazhin<br>Tel: +7 905 8744194<br>Skype: victor.knyazhin<br>ICQ: 328830376", Label.CONTENT_XHTML);
		Label contactLabel = new Label("By Victor Knyazhin", Label.CONTENT_XHTML);
		contactLabel.setWidth("100%");
		contactLabel.setHeight(SIZE_UNDEFINED, 0);
		addComponent(contactLabel);
		setWidth("100%");
		setHeight(SIZE_UNDEFINED, 0);
		setWidth("100%");
		setHeight(SIZE_UNDEFINED, 0);
		((VerticalLayout)getContent()).setSpacing(true);
	}
	
	//somewhy we can't use main window to download files to the user
	//so we are going to use this invisible window
	private Window downloadWindow = new Window("Download");
}
