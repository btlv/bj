package webbbsc;

interface IStrategyAcceptor {
	/**
	 * @param strategyFull: strategy translated to a strings array. Array length is possibleDealerHandsCount*yourHandsBeingConsideredCount
	 * currently, we consider the following set of our possible hands: 
	 * {"5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "S13", "S14", "S15", "S16", "S17", "S18", "S19", "S20",
	 * "2, 2", "3, 3", "4, 4", "5, 5", "6, 6", "7, 7", "8, 8", "9, 9", "T, T", "A, A"}
	 * may be a freedom to choose hands for this set should be added (adding/removing more or less obvious hands, which are included/not included now),
	 * so, currently array length is 10*32.
	 */
	public void acceptStrategy(String [] strategyFull);
	public void resetStrategy();
}

interface IRawStrategyAcceptor{
	public void acceptStrategy(byte [] rawStrategy);
}