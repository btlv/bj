package webbbsc;

import com.vaadin.ui.Button;
import com.vaadin.ui.Window;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

@SuppressWarnings("serial")
class ProgressWindow extends Window implements IProgressable{
	private ProgressIndicator pi = new ProgressIndicator();
	
	public ProgressWindow(String caption){
		super(caption);
	}
	
	private Button cancelButton = new Button("Cancel", new Button.ClickListener() {
		public void buttonClick(ClickEvent event) {
			ProgressWindow.this.getParent().removeWindow(ProgressWindow.this);
		}
	} );

	public void init(Window mainWindow){
        setModal(true);
        setClosable(false);
        setResizable(false);
        VerticalLayout layout = (VerticalLayout)getContent();
        layout.setMargin(true);
        layout.setSpacing(true);
        pi.setIndeterminate(false);
        pi.setEnabled(true);
        layout.addComponent(pi);
        layout.addComponent(cancelButton);
        mainWindow.addWindow(this);
	}

	public void setProgress(float progress){
		if(progress < 0f)
			progress = 0f;
		synchronized(getApplication()){
			pi.setValue(progress);
			if(progress >= 1f)
				getParent().removeWindow(this);
		}
	}
	
	public float getProgress(){
		return Float.parseFloat(pi.getValue().toString());
	}
}
