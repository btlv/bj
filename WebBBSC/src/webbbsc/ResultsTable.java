package webbbsc;

import com.vaadin.ui.Table;
import com.vaadin.ui.Label;

/**
 * @author Victor
 * A table designed to display strategies
 */
@SuppressWarnings("serial")
class ResultsTable extends Table {
	public void init(){
		setSelectable(false);
		setSortDisabled(true);
		addContainerProperty("0", String.class, null, "", null, null);
		for(String s: columns){
			addContainerProperty(s, Label.class, null);
		}
		resetResults();
		setCellStyleGenerator(new CommonStyleGenerator(this));
		setWidth("100%");
		setHeight("100%");
	}

	public void setResults(String [] strategyFull){
		removeAllItems();
		for(int i = 0; i < rows.length; ++i){
			Object [] row = new Object []{rows[i], null, null, null, null, null, null, null, null, null, null};
			for(int j = 0; j < columns.length; ++j){
				String actionsTable = strategyFull[j*rows.length + i]; 
				Label cell = new Label("" + actionsTable.charAt(0));
				cell.setDescription("possible actions and their mean yields<br>" +
				actionsTable.replace("\n", "<br>"));
				row[j + 1] = cell;
			}
			addItem(row, new Integer(i));
		}
	}
	
	public void resetResults(){
		removeAllItems();
		int i = 0;
		for(String s: rows){
			addItem(new Object[] {s, null, null, null, null, null, null, null, null, null, null}, new Integer(i++));
		}		
	}
	
	private final String [] columns = {"2", "3", "4", "5", "6", "7", "8", "9", "T", "A"};
	private final String [] rows = {"5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18",
			"S13", "S14", "S15", "S16", "S17", "S18", "S19", "S20",
			"2, 2", "3, 3", "4, 4", "5, 5", "6, 6", "7, 7", "8, 8", "9, 9", "T, T", "A, A"};
	
}
