package webbbsc;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;


/**
 * @author Victor
 * Right group of UI elements. Displays strategies.
 */
@SuppressWarnings("serial")
class RightGroup extends Panel implements IStrategyAcceptor{
	public void init(){
		results.init();
		legend.init();
		setSizeFull();
		setCaption("Basic Strategy");
		hLabel.setWidth(SIZE_UNDEFINED, 0);
		hLabel.setHeight(SIZE_UNDEFINED, 0);
		vLabel.setWidth(SIZE_UNDEFINED, 0);
		vLabel.setHeight(SIZE_UNDEFINED, 0);
		verticalGroup.addComponent(hLabel);
		verticalGroup.addComponent(results);
		verticalGroup.addComponent(legend);
		verticalGroup.setComponentAlignment(results, Alignment.TOP_CENTER);
		verticalGroup.setComponentAlignment(hLabel, Alignment.TOP_CENTER);
		verticalGroup.setSpacing(true);
		verticalGroup.setExpandRatio(results, 1.0f);
		mainLayout.addComponent(vLabel);
		mainLayout.addComponent(verticalGroup);
		mainLayout.setComponentAlignment(vLabel, Alignment.MIDDLE_CENTER);
		mainLayout.setComponentAlignment(verticalGroup, Alignment.MIDDLE_CENTER);
		mainLayout.setExpandRatio(verticalGroup, 1.0f);
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		setContent(mainLayout);
		mainLayout.setSizeFull();
		verticalGroup.setSizeFull();
	}

	//IStrategyAcceptor implementation//////////////////////
	public void acceptStrategy(String [] strategyFull){
		synchronized(getApplication()){
			if(strategyFull.length > 0 )
				results.setResults(strategyFull);
		}
	}
	
	public void resetStrategy(){
		results.resetResults();
	}
	////////////////////////////////////////////////////////
	
	private ResultsTable results = new ResultsTable();
	private LegendTable legend = new LegendTable();
	private VerticalLayout verticalGroup = new VerticalLayout();
	private HorizontalLayout mainLayout = new HorizontalLayout();
	private Label hLabel = new Label("Dealer up Card");
	private Label vLabel = new Label("Y<br>o<br>u<br>r<br><br>H<br>a<br>n<br>d", Label.CONTENT_XHTML);
}
