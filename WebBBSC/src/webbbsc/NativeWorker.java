package webbbsc;

/**
 * @author Victor
 * Interface to a native (written in c++, multithreaded) toolkit for calculating Black Jack base strategies 
 */
public class NativeWorker extends Thread{
//public:
	/**
	 * @param acc: the object to receive strategy in serialized form on calculation finish
	 */
	public void attachStrategyAcceptor(IRawStrategyAcceptor acc){
		acceptor = acc;
	}

	@Override
	public void run(){
		if(progress != null)
			progress.setProgress(0f);
		if(!prepareCalculation())
			return;
		
		//buildNextPiece() is by far the most CPU time consuming here
		while(!stopped && buildNextPiece()){
			if(!stopped){
				float currentProgress = getProgress();
				if(currentProgress >= 1.0f && acceptor != null)
					acceptor.acceptStrategy(getRawStrategy());
				if(progress != null)
					progress.setProgress(currentProgress);
			}
		}
	}

	public void setRules(Rules newRules){
		rules = newRules;
	}

	public void inhibit(boolean stop){
		stopped = stop;
	}

	public void terminate(){
		inhibit(true);
		while(isAlive()){
			try{
				join(1000);
			}catch(InterruptedException e){
			}
			interrupt();
		}
		releaseCalculator();
	}

	/**
	 * @param progr: interface for calling back progress changes
	 */
	public void attachProgress(IProgressable progr){
		progress = progr;
	}

//private:
	private native void createCalculator();
	private native void releaseCalculator();

	private Rules rules = new Rules();

	private long calculatorHandle = 0;

	private volatile boolean stopped = false;
	private IProgressable progress;
	private IRawStrategyAcceptor acceptor;

	private native boolean buildNextPiece();
	private native boolean prepareCalculation();

	//progress value is between 0 and 1
	private native float getProgress();

	private native byte[] getRawStrategy();


	static {
		try {
			System.loadLibrary("BJEngine");
		} catch (UnsatisfiedLinkError e) {
			System.err.println("Native code library failed to load: " + e);
		}
	}
}
