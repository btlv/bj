package webbbsc;

import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;

/**
 * @author Victor
 * Dispatcher layer. When asked for a strategy, extracts strategies from DB when possible, launches strategies calculations
 * when needed, inserts results into DB, so that no calculations need to be performed on the same strategies in future. 
 */
class DataDispatcher implements IRawStrategyAcceptor{
//public:
	public DataDispatcher(Window mainWindow){
		this.mainWindow = mainWindow;
	}
	/* (non-Javadoc)
	 * @see webbbsc.IRawStrategyAcceptor#acceptStrategy(byte[])
	 * callback method intended to be called from where the strategy got prepared
	 */
	public void acceptStrategy(byte [] rawStrategy){
		if(!DB.isThere(rules))
			DB.insert(rules, rawStrategy);
		higherAcceptor.acceptStrategy(NativeStrategyTranslator.getDetailedStrategy(rawStrategy));
	}
	
	/**
	 * Needs to be called on application close. Releases native resources.
	 */
	public void close(){
		cleanAfterNative();
	}
	
	/**
	 * Checks strategy availability in the DB, if strategy not found, launches calculation in a separate thread,
	 * opens a progress window, returns immediately. A newly calculated strategy gets inserted into DB via this.acceptStrategy().
	 * @param rulez
	 * @param acceptor: Object to receive prepared translated strategy e.g. a UI element designed to display strategies
	 */
	@SuppressWarnings("serial")
	public void getStrategy(Rules rulez, IStrategyAcceptor acceptor){
		rules = rulez;
		higherAcceptor = acceptor;
		byte [] storedStrategy = DB.select(rules);
		if(storedStrategy != null){
			higherAcceptor.acceptStrategy(NativeStrategyTranslator.getDetailedStrategy(storedStrategy));
			return;
		}
		
		prepareWorker();
		final ProgressWindow progress = new ProgressWindow("Calculation Progress");
		progress.init(mainWindow);
		progress.addListener(new Window.CloseListener() {
			public void windowClose(CloseEvent e) {
				if(progress.getProgress() < 1.f)
					nativeWorker.inhibit(true);
			}
		});

		nativeWorker.attachProgress(progress);
		nativeWorker.start();
	}
	
//private:
	private NativeWorker nativeWorker;
	private Rules rules;
	private IStrategyAcceptor higherAcceptor;
	private Window mainWindow;

	private void prepareWorker(){
		if(nativeWorker != null)
			cleanAfterNative();
		nativeWorker = new NativeWorker();
		nativeWorker.setRules(rules);
		nativeWorker.attachStrategyAcceptor(this);
	}
	
	private void cleanAfterNative(){
		if(nativeWorker != null)
			nativeWorker.terminate();
	}

}
