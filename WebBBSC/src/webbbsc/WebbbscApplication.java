package webbbsc;

import com.vaadin.Application;
import com.vaadin.ui.*;
import com.vaadin.ui.Window;

import com.vaadin.ui.Window.CloseEvent;

@SuppressWarnings("serial")
public class WebbbscApplication extends Application {
	private HorizontalLayout mainLayout = new HorizontalLayout();
	private GridLayout borderLayout = new GridLayout(1, 1);
	private LeftGroup leftGroupLayout = new LeftGroup();
	private RightGroup rightGroup = new RightGroup();
	
    @Override
	public void init() {
    	setTheme("webbbsctheme");
    	borderLayout.setSizeFull();
    	borderLayout.addComponent(mainLayout, 0, 0);
    	borderLayout.setComponentAlignment(mainLayout, Alignment.MIDDLE_CENTER);
    	borderLayout.setMargin(true);

    	rightGroup.init();
    	leftGroupLayout.init(rightGroup);

		mainLayout.setWidth("80%");
		mainLayout.setHeight("100%");
		mainLayout.addComponent(leftGroupLayout);
		mainLayout.addComponent(rightGroup);
		mainLayout.setComponentAlignment(leftGroupLayout, Alignment.TOP_CENTER);
		mainLayout.setComponentAlignment(rightGroup, Alignment.TOP_CENTER);
		mainLayout.setExpandRatio(leftGroupLayout, 1.f);
		mainLayout.setExpandRatio(rightGroup, 3.f);
		mainLayout.setSpacing(true);

		Window mainWindow = new Window("BlackJack Basic Strategies Calculator", borderLayout);
		mainWindow.setSizeFull();

		mainWindow.addListener(new Window.CloseListener(){
			public void windowClose(CloseEvent e) {
				leftGroupLayout.close();
				getMainWindow().getApplication().close();
			} 
		});

		setMainWindow(mainWindow);
	}
}
