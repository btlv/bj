package webbbsc;

import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
class LeftGroup extends VerticalLayout {
//public:
	public void init(IStrategyAcceptor acceptor){
		rightPanel = acceptor;
		newCalculationButton.setEnabled(false);
		startCalculationButton.setEnabled(true);
		newCalculationButton.setWidth("100%");
		startCalculationButton.setWidth("100%");
		rulesPanel.init();
		rulesPanel.setWidth("100%");
		verticalSpacer.setHeight("100%");
		infoPanel.init();
		infoPanel.setWidth("100%");
		addComponent(rulesPanel);
		addComponent(newCalculationButton);
		addComponent(startCalculationButton);
		addComponent(verticalSpacer);
		addComponent(infoPanel);
		setExpandRatio(verticalSpacer, 1.0f);	
		setHeight("100%");
	}
	
	public void close(){
		if(dataDispatcher != null)
			dataDispatcher.close();
	}

//private:
	private DataDispatcher		dataDispatcher;
	private IStrategyAcceptor 	rightPanel;
	private RulesPanel			rulesPanel = new RulesPanel();
	private AdditionalInfo		infoPanel = new AdditionalInfo();
	private Label				verticalSpacer = new Label();

	private Button	newCalculationButton = new Button("Change Rules", new Button.ClickListener() {
		
		public void buttonClick(ClickEvent event) {
			rulesPanel.setEnabled(true);
			newCalculationButton.setEnabled(false);
			startCalculationButton.setEnabled(true);
			rightPanel.resetStrategy();
		}
	} );
	
	private Button	startCalculationButton = new Button("Get the Strategy", new Button.ClickListener() {
		public void buttonClick(ClickEvent event) {
			rulesPanel.setEnabled(false);
			rulesPanel.requestRepaint();
			newCalculationButton.setEnabled(true);
			startCalculationButton.setEnabled(false);
			dataDispatcher = new DataDispatcher(getApplication().getMainWindow());
			dataDispatcher.getStrategy(rulesPanel.getRules(), rightPanel);
		}
	} );
}
