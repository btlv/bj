package webbbsc;

import com.vaadin.ui.Table;

/**
 * @author Victor
 * Returns style name for cells based on their contents
 * Suitable for main table displaying strategy, and for legend table
 */
public class CommonStyleGenerator implements Table.CellStyleGenerator {

	private Table table;
	private static final long serialVersionUID = 1L;

	public CommonStyleGenerator(Table owner){
		table = owner;
	}
	
	public String getStyle(Object itemId, Object propertyId) {
		if(itemId != null && propertyId != null)
		{
			int col = -1;
			//first, check for header column
			try{
				col = Integer.parseInt((String)propertyId);
				// The first column.
				if (col == 0)
					return "rowheader";
			}catch(NumberFormatException e){
				//Actually, a standard situation, no actions needed
			}
			Object cellObj = table.getItem(itemId).getItemProperty(propertyId);
			if(cellObj != null && null != cellObj.toString()){
				char cell = cellObj.toString().charAt(0);
				if(cell == 'H')
					return "hit";
				if(cell == 'S')
					return "stand";
				if(cell == 'D')
					return "double";
				if(cell == 'P')
					return "split";
				if(cell == 'R')
					return "surrender";
			}
		}
		return "last";
	}

}
