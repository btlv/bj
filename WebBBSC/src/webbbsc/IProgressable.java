package webbbsc;

/**
 * @author Victor
 * Interface for calling back progress
 */
interface IProgressable {
	public void setProgress(float progress);
}
