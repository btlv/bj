package webbbsc;

import com.vaadin.ui.Table;

/**
 * @author Victor
 * Small class for displaying strategy legend
 */
@SuppressWarnings("serial")
class LegendTable extends Table {
	public void init(){
		setWidth("100%");
		setHeight(SIZE_UNDEFINED, 0);
		setSelectable(false);
		setSortDisabled(true);
		setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
		setPageLength(1);
		setStyleName("legend");
		for(String s: columns)
			addContainerProperty(s, String.class, null);
		for(String s: columns)
			setColumnExpandRatio(s, 1.0f);
		addItem(columns, new Integer(0));
		setCellStyleGenerator(new CommonStyleGenerator(this));
	}
	private final String [] columns = {"H: Hit", "S: Stand", "D: Double", "P: Split", "R: Surrender"};
}
