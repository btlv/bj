package webbbsc;

import com.vaadin.ui.Panel;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.OptionGroup;
import org.vaadin.risto.stepper.IntStepper;
import com.vaadin.data.validator.RegexpValidator;

/**
 * @author Victor
 * A UI element for displaying and editing game rules.
 */
@SuppressWarnings("serial")
class RulesPanel extends Panel {

	public Rules getRules(){
		Rules rules = new Rules();
		rules.acesResplitAllowed = acesResplit.booleanValue();
		rules.dblAfterSplitAllowed = doubleAfterSplit.booleanValue();
		rules.dealerHitsS17 = dealerHitsS17Chbx.booleanValue();
		rules.dealerPeeks = dealerPeeks.booleanValue();
		rules.setMaxSplits((Integer)maxSplits.getValue());
		try{
			rules.dbl = Rules.DoubleType.fromString((String)doubleType.getValue());
		}
		catch(IllegalArgumentException e){
			System.err.println("Illegal argument: " + e);
		}
		try{
			rules.surr = Rules.Surrender.fromString((String)surrenderType.getValue());
		}
		catch(IllegalArgumentException e){
			System.err.println("Illegal argument: " + e);
		}
		return rules;
	}

	public void init(){
		setCaption("Rules");
		for(Rules.DoubleType d: Rules.DoubleType.values())
			doubleType.addItem(d.toString());
		doubleType.select(Rules.DoubleType.DAny.toString());

		for(Rules.Surrender s: Rules.Surrender.values())
			surrenderType.addItem(s.toString());
		surrenderType.select(Rules.Surrender.SNo.toString());

		for(int i = 0; i < 3; ++i)
			horizontalRules[i] = new Label("<HR>", Label.CONTENT_XHTML);

		addComponent(dealerHitsS17Chbx);
		addComponent(dealerPeeks);
		addComponent(horizontalRules[0]);
		addComponent(doubleType);
		addComponent(horizontalRules[1]);
		doubleAfterSplit.setValue(true);
		addComponent(doubleAfterSplit);
		maxSplits.setMinValue(Rules.MinMaxSplits);
		maxSplits.setMaxValue(Rules.MaxMaxSplits);
		
		maxSplits.setValue(3);
		maxSplits.setStepAmount(1);
		maxSplitsLabel.setWidth("100%");
		maxSplits.setWidth("90%");
		maxSplits.addValidator(new RegexpValidator("[0-5]", "Please enter an integer 0 - 5"));
		maxSplits.setImmediate(true);
		testLabel.setWidth("4px");
		maxSplitsContainer.addComponent(testLabel);
		maxSplitsContainer.addComponent(maxSplits);
		maxSplitsContainer.addComponent(maxSplitsLabel);
		maxSplitsContainer.setWidth("100%");
		maxSplitsContainer.setExpandRatio(maxSplits, 1.0f);
		maxSplitsContainer.setExpandRatio(maxSplitsLabel, 1.0f);
		addComponent(maxSplitsContainer);
		addComponent(acesResplit);
		addComponent(horizontalRules[2]);
		addComponent(surrenderType);

	}

	//private VerticalLayout	mainLayout = new VerticalLayout();
	private CheckBox		dealerHitsS17Chbx = new CheckBox("Dealer Hits Soft 17");
	private CheckBox		dealerPeeks = new CheckBox("Dealer Peeks for BJ");
	private CheckBox		doubleAfterSplit = new CheckBox("Double After Split");
	private CheckBox		acesResplit = new CheckBox("Aces Resplit");
	private OptionGroup		doubleType = new OptionGroup();
	private OptionGroup		surrenderType = new OptionGroup();
	private IntStepper		maxSplits = new IntStepper();
	private HorizontalLayout maxSplitsContainer = new HorizontalLayout();
	private Label			maxSplitsLabel = new Label("Max Splits");
	private Label[]  		horizontalRules = new Label[3];
	private Label			testLabel = new Label(" ");

}
