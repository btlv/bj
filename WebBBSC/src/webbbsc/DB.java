package webbbsc;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.logging.Logger;


/**
 * @author Victor
 * DB consists of a single table. Each rules field is a column, serialized strategy (blob) is also a column
 */
class DB {
//public:
	/**
	 * @param rules: Rules for which we want a strategy
	 * @return strategy: in serialized form, null if strategy for given rules not found or something goes wrong
	 */
	public static byte [] select(Rules rules){
		byte [] res = null;
		if(driverLoaded) {
			Connection conn = null;
			Statement stmt = null;
			try{
				conn = DataAccessFactory.getConnection();
				stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT Strategy " + makeFromStatement(rules));
				if(rs != null && rs.first()) {
					res = rs.getBytes(1);
					rs.close();
				}
			}
			catch(SQLException e){
				log.warning("Failed to establish pool connection: " + e.toString());
			}
			finally{
				releaseDbResources(conn, stmt);
			}
		}
		return res;
	}
	
	/**
	 * Inserts a strategy to DB
	 * Doesn't check if there is already a strategy in the DB for the rules provided
	 * use isThere for that purpose
	 * @param rules
	 * @param rawStrategy: strategy in serialized form
	 */
	public static void insert(Rules rules, byte [] rawStrategy){
		if(driverLoaded) {
			Connection conn = null;
			PreparedStatement stmt = null;
			try{
				conn = DataAccessFactory.getConnection();
				String query = new String("INSERT INTO " + tableName +
						" (DoubleType, MaxSplits, Surrender, DealerHitsS17, DealerPeeks, DblAfterSplit, AcesResplit, Strategy)" +
						" VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, rules.dbl.ordinal());
				stmt.setInt(2, rules.maxSplits);
				stmt.setInt(3, rules.surr.ordinal());
				stmt.setInt(4, rules.dealerHitsS17 ? 1 : 0);
				stmt.setInt(5, rules.dealerPeeks ? 1 : 0);
				stmt.setInt(6, rules.dblAfterSplitAllowed ? 1 : 0);
				stmt.setInt(7, rules.acesResplitAllowed ? 1 : 0);
				stmt.setBytes(8, rawStrategy);
				stmt.execute();
			}
			catch(SQLException e){
				log.warning("Failed to establish pool connection: " + e.toString());
			}
			finally{
				releaseDbResources(conn, stmt);
			}
		}
	}
	
	/**
	 * @param rules
	 * @return false if something goes wrong
	 */
	public static boolean isThere(Rules rules){
		boolean res = false;
		if(!driverLoaded)
			return false;
		Connection conn = null;
		Statement stmt = null;
		try{
			conn = DataAccessFactory.getConnection();
			stmt = conn.createStatement();
			res = stmt.executeQuery("SELECT AcesResplit " + makeFromStatement(rules)).first();
		}
		catch(SQLException e){
			log.warning("Failed to establish pool connection: " + e.toString());
		}
		finally{
			releaseDbResources(conn, stmt);
		}
		return res;
	}
//private:
	private static void releaseDbResources(Connection conn, Statement stmt){
		if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException sqlex) {
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException sqlex) {
            }
        }
	}
    private static String	tableName = new String("Strats");
    private static Logger	log = Logger.getLogger(DB.class.getName());
	private static boolean	driverLoaded = true;

    private static String makeFromStatement(Rules rules){
    	String res = new String(" FROM " + tableName +
    			" WHERE DoubleType = " + rules.dbl.ordinal() +
    			" AND MaxSplits = " + rules.maxSplits +
    			" AND Surrender = " + rules.surr.ordinal() +
    			" AND DealerHitsS17 = " + (rules.dealerHitsS17 ? 1 : 0) +
    			" AND DealerPeeks = " + (rules.dealerPeeks ? 1 : 0) +
    			" AND DblAfterSplit = " + (rules.dblAfterSplitAllowed ? 1 : 0) +
    			" AND AcesResplit = " + (rules.acesResplitAllowed ? 1 : 0) +
    			";");
    	return res;
    }

    static{
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		}
		catch(Exception ex){
			driverLoaded = false;
		}
	}
}

class JDBCUtils {
	private DataSource dataSource;

	public JDBCUtils() {
	}

	public void init(String dataSourceName) {
		try {
			InitialContext initContext = new InitialContext();
			dataSource = (DataSource) initContext.lookup(dataSourceName);
		} catch (NamingException e) {
		}
	}

	public Connection getConnection() throws SQLException {
		if (dataSource == null) {
			throw new SQLException("DataSource is null.");
		}

		return dataSource.getConnection();
	}

}

class DataAccessFactory {

	private static final DataAccessFactory instance = new DataAccessFactory();
	private JDBCUtils jdbcUtil;

	private DataAccessFactory() {
	}

	public static DataAccessFactory getInstance() {
		return instance;
	}

	private JDBCUtils prepareJDBCUtils() {
		if (jdbcUtil == null) {
			jdbcUtil = new JDBCUtils();
			jdbcUtil.init("jdbc/ConnectionPool");//jdbcUtil.init("java:comp/env/jdbc/ConnectionPool");
		}

		return jdbcUtil;
	}

	public static synchronized Connection getConnection() throws SQLException{
		return getInstance().prepareJDBCUtils().getConnection();
	}
}

