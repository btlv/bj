package webbbsc;

/**
 * @author Victor
 * Translates serialized strategies to a human readable text form
 * actual work performed by native methods
 */
class NativeStrategyTranslator {
	/**
	 * @param rawStrategy
	 * @return: simplified strategy - a string each character of which designates the best decision for a given situation
	 * H = hit
	 * S = stand
	 * D = double
	 * P = split
	 * R = surrender
	 * situation is given by character position in the string
	 * position = j*yourHandsBeingConsideredCount + i;
	 * j : [0, possibleDealerHandsCount), possibleDealerHandsCount = 10
	 * i : [0, yourHandsBeingConsideredCount), currently yourHandsBeingConsideredCount = 32
	 * oder of characters corresponds to j loop inside of i loop 
	 */
	public static native String getStrategy(byte [] rawStrategy);
	
	/**
	 * @param rawStrategy
	 * @return see getStrategy description, the only difference is now more detailed description produces,
	 * it's a separate string, not a single character
	 */
	public static native String[] getDetailedStrategy(byte [] rawStrategy);

	static {
		try {
			System.loadLibrary("BJEngine");
		} catch (UnsatisfiedLinkError e) {
			System.err.println("Native code library failed to load: " + e);
		}
	}

}
