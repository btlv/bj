#include "webbbsc_NativeWorker.h"
#include "calculatorsRegistry.h"
#include "../../BJEngine/src/StrategyTranslator.h"
#include "../../BJEngine/src/Core.h"
#include "../../BJEngine/src/Rules.h"
#include "../../BJEngine/src/IStrategyCalculator.h"
#include <string>
#include <exception>
#include <vector>
#include <sstream>

static int parseEnum(JNIEnv* env, jobject enumObj, jclass enumClass, const std::vector<std::string>& names)
{
	jmethodID getNameMethod = env->GetMethodID(enumClass, "name", "()Ljava/lang/String;");
	jstring strVal = (jstring)env->CallObjectMethod(enumObj, getNameMethod);
	std::string valueNative = env->GetStringUTFChars(strVal, 0);
	int i = 0;
	for(int size = names.size(); i < size; ++i)
		if(valueNative == names[i])
			break;
	return i;
}

static CalculatorHandle getCalculatorHandle(JNIEnv* env, jobject workerObj)
{
	jclass cls = env->GetObjectClass(workerObj);
	jfieldID fid = env->GetFieldID(cls, "calculatorHandle", "J");
	return (CalculatorHandle)env->GetLongField(workerObj, fid);
}

static void setCalculatorHandle(JNIEnv* env, jobject workerObj, CalculatorHandle hndl)
{
	jclass cls = env->GetObjectClass(workerObj);
	jfieldID fid = env->GetFieldID(cls, "calculatorHandle", "J");
	env->SetLongField(workerObj, fid, hndl);
}

//Checks if there is already a calculator attached to this java worker
//if there isn't, creates a new one
static IStrategyCalculatorPtr getCalculator(JNIEnv* env, jobject workerObj)
{
	IStrategyCalculatorPtr calc((IStrategyCalculator*)NULL);
	CalculatorHandle hndl = getCalculatorHandle(env, workerObj);
	if(hndl)
		calc = CalculatorsRegistry::GetRegistry().Get(hndl);
	else
	{
		calc = Core::GetCore().NewStrategyCalculator();
		hndl = CalculatorsRegistry::GetRegistry().Store(calc);
		setCalculatorHandle(env, workerObj, hndl);
	}
	return calc;
}

static void translateRules(JNIEnv* env, jobject self, Rules& rules)
{
	jfieldID rulesId;
	jclass cls = env->GetObjectClass(self);
	rulesId = env->GetFieldID(cls, "rules", "Lwebbbsc/Rules;");
	if(!rulesId)
		throw std::exception("rules field ID not found");
	jobject rulesObj = env->GetObjectField(self, rulesId);
	if(!rulesObj)
		throw std::string("rulesObj not found");

	cls = env->GetObjectClass(rulesObj);

	if(!cls)
		throw std::string("class is unknown");

	jfieldID fid = env->GetFieldID(cls, "maxSplits", "I");
	if(!fid)
		throw std::string("maxSplits not found");
	rules.maxSplits = env->GetIntField(rulesObj, fid);

	fid = env->GetFieldID(cls, "dealerHitsS17", "Z");
	if(!fid)
		throw std::string("dealerHitsS17 not found");
	rules.dealerHitsS17 = 0 != env->GetBooleanField(rulesObj, fid);

	fid = env->GetFieldID(cls, "dealerPeeks", "Z");
	if(!fid)
		throw std::string("dealerPeeks not found");
	rules.dealerPeeks = 0 != env->GetBooleanField(rulesObj, fid);

	fid = env->GetFieldID(cls, "dblAfterSplitAllowed", "Z");
	if(!fid)
		throw std::string("dblAfterSplitAllowed not found");
	rules.dblAfterSplitAllowed = 0 != env->GetBooleanField(rulesObj, fid);

	fid = env->GetFieldID(cls, "acesResplitAllowed", "Z");
	if(!fid)
		throw std::string("acesResplitAllowed not found");
	rules.acesResplitAllowed = 0 != env->GetBooleanField(rulesObj, fid);

	fid = env->GetFieldID(cls, "dbl", "Lwebbbsc/Rules$DoubleType;");
	if(!fid)
		throw std::string("double type not found");
	jobject enumObj = env->GetObjectField(rulesObj, fid);
	jclass enumClass = env->FindClass("webbbsc/Rules$DoubleType");
	std::vector<std::string> names;
	names.push_back("DAny");
	names.push_back("DOnly_9_10_11");
	names.push_back("DOnly_10_11");
	int enumVal = parseEnum(env, enumObj, enumClass, names);
	if(enumVal < Rules::DLast)
		rules.dbl = (Rules::Double)enumVal;
	else
		throw std::string("unknown double type");

	fid = env->GetFieldID(cls, "surr", "Lwebbbsc/Rules$Surrender;");
	if(!fid)
		throw std::string("surrender type not found");
	enumObj = env->GetObjectField(rulesObj, fid);
	enumClass = env->FindClass("webbbsc/Rules$Surrender");
	names.clear();
	names.push_back("SNo");
	names.push_back("SLate");
	names.push_back("SEarly");
	enumVal = parseEnum(env, enumObj, enumClass, names);
	if(enumVal < Rules::SLast)
		rules.surr = (Rules::Surrender)enumVal;
	else
		throw std::string("unknown surrender type");

	Core::GetCore().GetLog() << "double type = " << rules.dbl << std::endl
		<< "surrender type = " << rules.surr << std::endl;
}

JNIEXPORT void JNICALL Java_webbbsc_NativeWorker_run
(JNIEnv *env, jobject self)
{
}

JNIEXPORT jfloat JNICALL Java_webbbsc_NativeWorker_getProgress
(JNIEnv *env, jobject workerObj)
{
	return getCalculator(env, workerObj)->BuiltPart();
}

JNIEXPORT void JNICALL Java_webbbsc_NativeWorker_createCalculator
(JNIEnv *env, jobject self)
{
	getCalculator(env, self);
}

JNIEXPORT void JNICALL Java_webbbsc_NativeWorker_releaseCalculator
(JNIEnv *env, jobject self)
{
	CalculatorHandle hndl = getCalculatorHandle(env, self);
	if(hndl)
		CalculatorsRegistry::GetRegistry().Release(hndl);
}

JNIEXPORT jboolean JNICALL Java_webbbsc_NativeWorker_prepareCalculation
(JNIEnv *env, jobject self)
{
	Rules rules;
	try
	{
		translateRules(env, self, rules);
	}
	catch(std::string& errorMsg)
	{
		Core::GetCore().GetLog() << "Failed to start calculation: " << errorMsg << std::endl;
		return false;
	}
	IStrategyCalculatorPtr calc = getCalculator(env, self);
	calc->Reset(rules);
	return true;
}

JNIEXPORT jboolean JNICALL Java_webbbsc_NativeWorker_buildNextPiece
(JNIEnv *env, jobject self)
{
	IStrategyCalculatorPtr calc = getCalculator(env, self);
	if(calc)
		return calc->BuildNextPiece();
	return false;
}

JNIEXPORT jbyteArray JNICALL Java_webbbsc_NativeWorker_getRawStrategy
(JNIEnv *env, jobject self)
{
	IStrategyCalculatorPtr calc = getCalculator(env, self);
	const GameActionsLib& lib = calc->GetGameActionsLib();
	std::ostringstream os;
	lib.Serialize(os);
	os.flush();
	size_t size = os.tellp();
	Core::GetCore().GetLog() << "serialized strategy size = " << size << std::endl;
	jbyteArray jb = env->NewByteArray(size);
	//TODO: get rid of redundant serialized GameActionsLib copying
	env->SetByteArrayRegion(jb, 0, size, (jbyte *)(&os.str()[0]));
	return jb;
}
