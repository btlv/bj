#include "webbbsc_NativeStrategyTranslator.h"
#include "../../BJEngine/src/StrategyTranslator.h"
#include "../../BJEngine/src/Core.h"
#include <streambuf>


class GameActionsLibJNI: public GameActionsLib
{
	struct StreamBuf: public std::streambuf
	{
		StreamBuf(char* buf, size_t size)
		{
			setg(buf, buf, buf + size);
		}
	};
public:
	GameActionsLibJNI(JNIEnv* env, jbyteArray rawStrategy)
	{
		size_t size = env->GetArrayLength(rawStrategy);
		Core::GetCore().GetLog() << "raw strategy size = " << size << std::endl;
		jbyte* buf = env->GetByteArrayElements(rawStrategy, NULL);
		StreamBuf strBuf((char*)buf, size);
		std::istream bufStream(&strBuf);
		UnSerialize(bufStream);
		env->ReleaseByteArrayElements(rawStrategy, buf, 0);
	}
};

JNIEXPORT jstring JNICALL Java_webbbsc_NativeStrategyTranslator_getStrategy
(JNIEnv *env, jclass clazz, jbyteArray rawStrategy)
{
	GameActionsLibJNI lib(env, rawStrategy);
	StrategyTranslatorSimple simpleTranslator(lib);
	simpleTranslator.translate();
	return env->NewStringUTF(simpleTranslator.getTranslation().c_str());
}

JNIEXPORT jobjectArray JNICALL Java_webbbsc_NativeStrategyTranslator_getDetailedStrategy
(JNIEnv *env, jclass clazz, jbyteArray rawStrategy)
{
	GameActionsLibJNI lib(env, rawStrategy);
	StrategyTranslator translator(lib);
	translator.translate();
	const std::vector<std::string>& translation = translator.getTranslation();
	jclass stringArrCls = env->FindClass("java/lang/String");
	jobjectArray res = env->NewObjectArray(translation.size(), stringArrCls, env->NewStringUTF(""));
	for(unsigned int i = 0, size = translation.size(); i < size; ++i)
		env->SetObjectArrayElement(res, i, env->NewStringUTF(translation[i].c_str()));
	return res;
}
