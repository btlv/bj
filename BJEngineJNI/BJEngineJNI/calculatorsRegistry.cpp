#include "calculatorsRegistry.h"

CalculatorsRegistry::CalculatorsRegistry(){}

CalculatorsRegistry::CalculatorsRegistry(const CalculatorsRegistry& another){}

CalculatorsRegistry& CalculatorsRegistry::GetRegistry()
{
	static CalculatorsRegistry theRegistry;
	return theRegistry;
}

CalculatorHandle CalculatorsRegistry::Store(IStrategyCalculatorPtr calc)
{
	CalculatorHandle hndl = (long)(void*)calc.get();
	storage_.insert(std::make_pair(hndl, calc));
	return hndl;
}

void CalculatorsRegistry::Release(CalculatorHandle handle)
{
	storage_.erase(handle);
}

IStrategyCalculatorPtr CalculatorsRegistry::Get(CalculatorHandle hndl)
{
	CalculatorsMap::iterator it = storage_.find(hndl);
	if(it != storage_.end())
		return it->second;
	return IStrategyCalculatorPtr((IStrategyCalculator*)NULL);
}