#ifndef _CLC_REGYSTRY_
#define _CLC_REGYSTRY_

#include <map>
#include "../../BJEngine/src/IStrategyCalculator.h"

typedef long CalculatorHandle;

class CalculatorsRegistry
{
	typedef std::map<CalculatorHandle, IStrategyCalculatorPtr> CalculatorsMap;
	CalculatorsMap storage_;

	CalculatorsRegistry();
	CalculatorsRegistry(const CalculatorsRegistry& another);
	~CalculatorsRegistry(){}

public:
	static CalculatorsRegistry& GetRegistry();
	CalculatorHandle Store(IStrategyCalculatorPtr calc);
	IStrategyCalculatorPtr Get(CalculatorHandle hndl);
	void Release(CalculatorHandle handle);
};
#endif