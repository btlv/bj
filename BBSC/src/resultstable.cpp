#include "resultstable.h"

#include "../../BJEngine/src/StrategyTranslator.h"

#include <QHeaderView>

ResultsTable::ResultsTable(QWidget* qw):
        QTableWidget(qw)
{
    horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    verticalHeader()->setResizeMode(QHeaderView::Stretch);
}

void ResultsTable::Fill(const GameActionsLib& lib)
{
    SituationDesc desc;
    assert(!lib.empty());

	StrategyTranslator	translator(lib);
	translator.translate();
	const std::vector<std::string>& translation = translator.getTranslation();

    for(size_t i = 0, sizeHoriz = columnCount(); i < sizeHoriz; ++i)
    {
        for(size_t j = 0, sizeVert = rowCount(); j < sizeVert; ++j)
        {
            QTableWidgetItem* it = item(j, i);
            if(!it)
            {
                it = new QTableWidgetItem;
                setItem(j, i, it);
            }
			const std::string& actionsTable = translation[i*sizeVert + j];
			char act = actionsTable[0];
            it->setText(QString(act));
			it->setBackgroundColor(GameActionToColor(GameAction::CharToGameAction(act)));
            QString details(tr("possible actions and their mean yields:\n"));
			details += actionsTable.c_str();
            it->setToolTip(details);
            details.replace(tr("\n"), tr(" "));
            it->setStatusTip(details);
        }
    }
}

void ResultsTable::Clear()
{
    for(size_t i = 0, sizeHoriz = columnCount(); i < sizeHoriz; ++i)
    {
        for(size_t j = 0, sizeVert = rowCount(); j < sizeVert; ++j)
        {
            QTableWidgetItem* it = item(j, i);
            if(!it)
                continue;
            it->setText(tr(""));
            it->setStatusTip(tr(""));
            it->setToolTip(tr(""));
            it->setBackground(GameActionToColor(GameAction::GALast));
        }
    }
}
