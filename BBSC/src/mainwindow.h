#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_

#include <QtGui/QMainWindow>
#include <QMutex>
#include <QProgressDialog>

#include <vector>
#include <memory>
#include <time.h>

#include "../../BJEngine/src/Rules.h"

#include "WorkerThread.h"
#include "DB.h"

namespace Ui
{
    class MainWindow;
}

class Timer
{
private:
    bool    isRunning_;
    clock_t startTime_;
public:
    Timer():
            isRunning_(false)
            , startTime_(0)
    {}

    void Start()
    {
        isRunning_ = true;
        startTime_ =  clock();
    }

    float Stop()
    {
        if(isRunning_)
        {
            isRunning_ = false;
            return (float)((clock() - startTime_)/(float)CLOCKS_PER_SEC);
        }
        return 0.f;
    }
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    std::auto_ptr<QProgressDialog> progress_;

    Rules                       rules_;
    WorkerThread                worker_;
    Timer                       timer_;

    DB                          db_;

    bool                        calculatedNotLoaded_;

    void ActivateRulesGroup(bool activate);

    void FillResultsTable();
    void RenewRulesGroup();

    bool ReadStrategyFile(const QString& filename);
    bool WriteStrategyFile(const QString& filename);

private slots:

    //names of standard slots are also standard
    void on_radioButton_DoubleAny_clicked();
    void on_radioButton_Double_9_10_11_clicked();
    void on_radioButton_Double_10_11_clicked();

    void on_radioButton_NoSur_clicked();
    void on_radioButton_EarlySur_clicked();
    void on_radioButton_LateSur_clicked();

    void on_checkBox_DealerHitsS17_clicked();
    void on_checkBox_DealerPeeks_clicked();
    void on_checkBox_DAS_clicked();
    void on_checkBox_AcesResplit_clicked();
    void on_maxSplits_valueChanged(int);

    void on_runButton_clicked();
    void on_newButton_clicked();

    void on_actionAbout_triggered();
    void on_actionSave_triggered();
    void on_actionLoad_triggered();

    //additional slots named the same way as general methods
    void CalculationFinished();
    void CancelCalculation();
};

#endif // MAINWINDOW_H
