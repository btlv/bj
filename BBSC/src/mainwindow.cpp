#include "mainwindow.h"
#include "../ui_mainwindow.h"

#include <fstream>

#include <QMessageBox>
#include <QFileDialog>
#include <QThread>

#include "resultstable.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , worker_(rules_)
    , calculatedNotLoaded_(true)
{
    ui->setupUi(this);
    QTableWidget* legend = ui->resultsLegend;
    for(size_t i = 0; i < legend->columnCount(); ++i)
        legend->item(0, i)->setBackgroundColor(ResultsTable::GameActionToColor((GameAction::GameAction)i));
    connect(&worker_, SIGNAL(CalculationFinished()), this, SLOT(CalculationFinished()));
    RenewRulesGroup();
    ui->resultsLegend->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    ui->resultsLegend->verticalHeader()->setResizeMode(QHeaderView::Stretch);
    ui->checkBox_AcesResplit->adjustSize();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_radioButton_DoubleAny_clicked()
{
    rules_.dbl = Rules::DAny;
}

void MainWindow::on_radioButton_Double_9_10_11_clicked()
{
    rules_.dbl = Rules::DOnly_9_10_11;
}

void MainWindow::on_radioButton_Double_10_11_clicked()
{
    rules_.dbl = Rules::DOnly_10_11;
}

void MainWindow::on_radioButton_NoSur_clicked()
{
    rules_.surr = Rules::SNo;
}

void MainWindow::on_radioButton_EarlySur_clicked()
{
    rules_.surr = Rules::SEarly;
}

void MainWindow::on_radioButton_LateSur_clicked()
{
    rules_.surr = Rules::SLate;
}

void MainWindow::on_checkBox_DealerHitsS17_clicked()
{
    rules_.dealerHitsS17 = !rules_.dealerHitsS17;
}

void MainWindow::on_checkBox_DealerPeeks_clicked()
{
    rules_.dealerPeeks = !rules_.dealerPeeks;
}

void MainWindow::on_checkBox_DAS_clicked()
{
    rules_.dblAfterSplitAllowed = !rules_.dblAfterSplitAllowed;
}

void MainWindow::on_checkBox_AcesResplit_clicked()
{
    rules_.acesResplitAllowed = !rules_.acesResplitAllowed;
}

void MainWindow::on_maxSplits_valueChanged(int newMax)
{
    rules_.maxSplits = newMax;
}

void MainWindow::on_runButton_clicked()
{
    ActivateRulesGroup(false);
    ui->actionSave->setEnabled(false);
    ui->actionLoad->setEnabled(false);
    ui->checkBox_UseDB->setEnabled(false);

    if(ui->checkBox_UseDB->isChecked()// && db_.IsOK()
		)
    {
        QApplication::setOverrideCursor(Qt::WaitCursor);
        GameActionsLib lib;
        const bool extractionSeccess = db_.Extract(rules_, lib);//this (Extract) operation may be quite time consuming
        QApplication::restoreOverrideCursor();
        if(extractionSeccess)
        {
            worker_.GetCalculator().Set(lib, rules_);
            calculatedNotLoaded_ = false;
            CalculationFinished();
            return;
        }
    }
    calculatedNotLoaded_ = true;
    progress_.reset(new QProgressDialog());
    progress_->setLabelText(tr("Calculating..."));
    progress_->setWindowTitle(tr("Calculation progress"));
    progress_->setRange(0, 100);
    progress_->setValue(0);
    progress_->setModal(true);
    progress_->show();

    timer_.Start();
    worker_.GetCalculator().Reset(rules_);
    worker_.start();
    connect(progress_.get(), SIGNAL(canceled()), SLOT(CancelCalculation()));
    connect(&worker_, SIGNAL(ProgressChanged(int)), progress_.get(), SLOT(setValue(int)));
}

void MainWindow::on_newButton_clicked()
{
    worker_.GetCalculator().Reset(rules_);
    ActivateRulesGroup(true);
    ui->actionSave->setEnabled(false);
    ui->newButton->setEnabled(false);
    ui->checkBox_UseDB->setEnabled(true);
    worker_.stop(false);

    ui->tableWidget->Clear();
    ui->statusBar->clearMessage();
}

void MainWindow::ActivateRulesGroup(bool activate)
{
    ui->checkBox_DAS->setEnabled(activate);
    ui->checkBox_DealerHitsS17->setEnabled(activate);
    ui->checkBox_DealerPeeks->setEnabled(activate);
    ui->checkBox_AcesResplit->setEnabled(activate);
    ui->radioButton_NoSur->setEnabled(activate);
    ui->radioButton_LateSur->setEnabled(activate);
    ui->radioButton_EarlySur->setEnabled(activate);
    ui->radioButton_Double_9_10_11->setEnabled(activate);
    ui->radioButton_Double_10_11->setEnabled(activate);
    ui->radioButton_DoubleAny->setEnabled(activate);
    ui->maxSplits->setEnabled(activate);
    ui->runButton->setEnabled(activate);
    ui->labelSplits->setEnabled(activate);
}

void MainWindow::CalculationFinished()
{
    ui->actionSave->setEnabled(true);
    ui->actionLoad->setEnabled(true);
    ui->newButton->setEnabled(true);
    FillResultsTable();
    if(calculatedNotLoaded_)
    {
        ui->statusBar->showMessage(tr("strategy generated in %1 seconds").arg(QString().setNum(timer_.Stop(), 'f', 1)));
        if(ui->checkBox_UseDB->isChecked()// && db_.IsOK()
			)
        {
            QApplication::setOverrideCursor(Qt::WaitCursor);
            db_.Store(rules_, worker_.GetCalculator().GetGameActionsLib());
            QApplication::restoreOverrideCursor();
        }
    }
}

void MainWindow::CancelCalculation()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    if(progress_.get())
        progress_->close();
    worker_.stop();
    worker_.wait();
    QApplication::restoreOverrideCursor();
    on_newButton_clicked();
    ui->actionLoad->setEnabled(true);
}

void MainWindow::FillResultsTable()
{
    ui->tableWidget->Fill(worker_.GetCalculator().GetGameActionsLib());
}

void MainWindow::RenewRulesGroup()
{
    ui->checkBox_DAS->setChecked(rules_.dblAfterSplitAllowed);
    ui->checkBox_DealerHitsS17->setChecked(rules_.dealerHitsS17);
    ui->checkBox_DealerPeeks->setChecked(rules_.dealerPeeks);
    ui->checkBox_AcesResplit->setChecked(rules_.acesResplitAllowed);

    QRadioButton* doubleRBs[] = {ui->radioButton_DoubleAny,
                                 ui->radioButton_Double_9_10_11,
                                 ui->radioButton_Double_10_11};//order must be same as in Rules::Double
    QRadioButton* surrRBs[] = {ui->radioButton_NoSur,
                               ui->radioButton_LateSur,
                               ui->radioButton_EarlySur};//order must be same as in Rules::Surrender
    assert(rules_.dbl < Rules::DLast);
    assert(rules_.surr < Rules::SLast);
    doubleRBs[rules_.dbl]->setChecked(true);
    surrRBs[rules_.surr]->setChecked(true);

    ui->maxSplits->setValue(rules_.maxSplits);
}

bool MainWindow::ReadStrategyFile(const QString& filename)
{
    std::ifstream file(filename.toStdString().c_str(), std::ios::in | std::ios::binary);
    if (!file.is_open())
    {
        QMessageBox::warning(this, tr("BBSC"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(filename)
                             .arg("unknown error"));
        return false;
    }
    QApplication::setOverrideCursor(Qt::WaitCursor);
    rules_.UnSerialize(file);
    GameActionsLib lib;
    lib.UnSerialize(file);
    worker_.GetCalculator().Set(lib, rules_);
    file.close();
    CalculationFinished();
    QApplication::restoreOverrideCursor();
    return true;
}

bool MainWindow::WriteStrategyFile(const QString& filename)
{
    std::ofstream file(filename.toStdString().c_str(), std::ios::out | std::ios::binary);
    if (!file.is_open())
    {
        QMessageBox::warning(this, tr("BBSC"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(filename)
                             .arg("unknown error"));
        return false;
    }
    QApplication::setOverrideCursor(Qt::WaitCursor);
    rules_.Serialize(file);
    worker_.GetCalculator().GetGameActionsLib().Serialize(file);
    file.flush();
    file.close();
    QApplication::restoreOverrideCursor();
    return true;
}

void MainWindow::on_actionSave_triggered()
{
    QString fName = QFileDialog::getSaveFileName(this,
                                                    tr("Save Basic Strategy"), ".",
                                                    tr("Basic Strategy files (*.bjs)"));
    if(!fName.isEmpty())
        WriteStrategyFile(fName);
}

void MainWindow::on_actionLoad_triggered()
{
    QString fName = QFileDialog::getOpenFileName(this,
                                                       tr("Load Basic Strategy"), ".",
                                                       tr("Basic Strategy files (*.bjs)"));
    if(!fName.isEmpty() && ReadStrategyFile(fName))
    {
        RenewRulesGroup();
        ActivateRulesGroup(false);
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("About Blackjack Basic Strategy Calculator"),
                       tr("<h2>Blackjack Basic Strategy Calculator 1.0</h2>"
                          "<p>Copyright &copy; 2009 Knyazhin V.S."
                          "<p>BBSC is a small application which "
                          "computes optimal blackjack base strategys. "
                          "Also demonstrates multithreaded solving of a "
                          "computationally demanding problem."));
}
