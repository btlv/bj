#ifndef _WORKERTHREAD_H_
#define _WORKERTHREAD_H_

#include <QThread>
#include "../../BJEngine/src/IStrategyCalculator.h"

class WorkerThread : public QThread
{
    Q_OBJECT
public:
    WorkerThread(const Rules& rules);
    ~WorkerThread()
    {
        QThread::exit();
        QThread::wait();
    }

    IStrategyCalculator& GetCalculator() {return *calculator_;}
    void stop(bool stop = true) {stopped_ = stop;}

signals:
    void CalculationFinished();
    void ProgressChanged(int progress);

protected:
    void run();

private:
    volatile bool               stopped_;
    IStrategyCalculatorPtr      calculator_;
};

#endif // WORKERTHREAD_H
