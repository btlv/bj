#ifndef DB_H
#define DB_H

#include <string>

#include <QSqlDatabase>

#include "../../BJEngine/src/Rules.h"
#include "../../BJEngine/src/Containers.h"

class DB
{
private:
    bool            isOK_;
    std::string     userName_;
    std::string     userPwd_;
    std::string     serverHostName_;
    std::string     dbName_;
    std::string     tableName_;
    QSqlDatabase    db_;

    void MakeFromStatement(const Rules& rules, QString& str) const;
    void CheckDBConnection();
    bool ReadIni();
public:
    DB();
    ~DB();

    bool IsOK() const {return isOK_;}
    bool IsThere(const Rules& rules);//true, if there is already data for such rules in the db
    void Store(const Rules& rules, const GameActionsLib& lib);
    bool Extract(const Rules& rules, GameActionsLib& lib);
};

#endif // DB_H
