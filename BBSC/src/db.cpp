#include "db.h"

#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

#include <strstream>
#include <fstream>
#include <string>
#include <limits>
#include <map>

DB::DB():
        isOK_(false)
{
    if(ReadIni())
    {
        db_ = QSqlDatabase::addDatabase("QMYSQL");
        db_.setHostName(serverHostName_.c_str());
        db_.setDatabaseName(dbName_.c_str());
        db_.setUserName(userName_.c_str());
        db_.setPassword(userPwd_.c_str());
        isOK_ = true;
    }
	else
	{
		QMessageBox::warning(0, QObject::tr("Database Error"), "Error reading database connection settings");
	}
}

bool DB::ReadIni()
{
    std::ifstream ini("db.ini", std::ios::in);
    if(ini.is_open())
    {
        typedef std::map<std::string, std::string*> SyntaxMap;
        SyntaxMap configSyntax;
        configSyntax.insert(std::make_pair<std::string, std::string*>("server_host:", &serverHostName_));
        configSyntax.insert(std::make_pair<std::string, std::string*>("db_name:", &dbName_));
        configSyntax.insert(std::make_pair<std::string, std::string*>("table_name:", &tableName_));
        configSyntax.insert(std::make_pair<std::string, std::string*>("user_name:", &userName_));
        configSyntax.insert(std::make_pair<std::string, std::string*>("user_password:", &userPwd_));
        std::string s;
        size_t readCount = 0;
		while(ini >> s)
        {
            SyntaxMap::const_iterator i = configSyntax.find(s);
            if(i != configSyntax.end())
            {
                ini >> *(i->second);
                ++readCount;
            }
			s.clear();
			ini.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        ini.close();
        if(readCount == configSyntax.size())
            return true;
    }
    return false;
}

DB::~DB()
{
    if(db_.isOpen())
        db_.close();
}

void DB::CheckDBConnection()
{
    if(!db_.isOpen() && !db_.open())
    {
        isOK_ = false;
        QMessageBox::warning(0, QObject::tr("Database Error"),
                              db_.lastError().text());
    }
}

void DB::MakeFromStatement(const Rules& rules, QString& str) const
{
    str = " FROM ";
    str += tableName_.c_str();
    str += " WHERE DoubleType = ";
    str += QString::number((unsigned int)rules.dbl);
    str += " AND MaxSplits = ";
    str += QString::number((unsigned int)rules.maxSplits);
    str += " AND Surrender = ";
    str += QString::number((unsigned int)rules.surr);
    str += " AND DealerHitsS17 = ";
    str += QString::number((unsigned int)rules.dealerHitsS17);
    str += " AND DealerPeeks = ";
    str += QString::number((unsigned int)rules.dealerPeeks);
    str += " AND DblAfterSplit = ";
    str += QString::number((unsigned int)rules.dblAfterSplitAllowed);
    str += " AND AcesResplit = ";
    str += QString::number((unsigned int)rules.acesResplitAllowed);
    str += ';';
}

void DB::Store(const Rules& rules, const GameActionsLib& lib)
{
    if(//!isOK_ ||
		IsThere(rules))
        return;

    QString statement("INSERT INTO ");
    statement += tableName_.c_str();
    statement += " (DoubleType, MaxSplits, Surrender, DealerHitsS17, DealerPeeks, DblAfterSplit, AcesResplit, Strategy)";
    statement += " VALUES (:DoubleType, :MaxSplits, :Surrender, :DealerHitsS17, :DealerPeeks, :DblAfterSplit, :AcesResplit, :Strategy)";
    QSqlQuery query(db_);
    if(!query.prepare(statement))
    {
        isOK_ = false;
        return;
    }
    query.bindValue(":DoubleType", (unsigned int)rules.dbl);
    query.bindValue(":MaxSplits", (unsigned int)rules.maxSplits);
    query.bindValue(":Surrender", (unsigned int)rules.surr);
    query.bindValue(":DealerHitsS17", (unsigned int)rules.dealerHitsS17);
    query.bindValue(":DealerPeeks", (unsigned int)rules.dealerPeeks);
    query.bindValue(":DblAfterSplit", (unsigned int)rules.dblAfterSplitAllowed);
    query.bindValue(":AcesResplit", (unsigned int)rules.acesResplitAllowed);

    std::ostrstream blob;
    lib.Serialize(blob);
    QByteArray bAr(blob.str(), blob.pcount());
    query.bindValue(":Strategy", bAr);

    query.exec();
    if(!query.isActive())
        isOK_ = false;
}

bool DB::IsThere(const Rules& rules)
{
    //if(!isOK_)
    //    return false;

    CheckDBConnection();
    QString statement;
    MakeFromStatement(rules, statement);
    statement = "SELECT AcesResplit" + statement;

    QSqlQuery query = db_.exec(statement);
    if(!query.isActive())
        return (isOK_ = false);

    if(query.size() > 0)
        return true;
    return false;
}

bool DB::Extract(const Rules& rules, GameActionsLib& lib)
{
    if(//!isOK_ ||
		!IsThere(rules))
        return false;

    QString statement;
    MakeFromStatement(rules, statement);
    statement = "SELECT Strategy" + statement;
    QSqlQuery query(db_);

    if(!query.exec(statement) || !query.isActive())
        return (isOK_ = false);
    if(query.size() > 0 && query.next())
    {
        QByteArray bAr = query.value(0).toByteArray();
        std::istrstream blob(bAr.data(), bAr.size());
        lib.UnSerialize(blob);
        return true;
    }
    return false;
}
