#ifndef _RESULTSTABLE_H_
#define _RESULTSTABLE_H_

#include <QTableWidget>

#include "../../BJEngine/src/Task.h"

class ResultsTable : public QTableWidget
{
private:
    static Card RowToHand(size_t row, size_t firstOrSecond)
    {
        static const Card rowToHand[][2] =
        {
            {Card3, Card2}
            , {Card4, Card2}
            , {Card5, Card2}
            , {Card6, Card2}
            , {Card7, Card2}
            , {Card8, Card2}
            , {Card9, Card2}
            , {Card10, Card2}
            , {Card10, Card3}
            , {Card10, Card4}
            , {Card10, Card5}
            , {Card10, Card6}
            , {Card10, Card7}
            , {Card10, Card8}
            , {CardA, Card2}
            , {CardA, Card3}
            , {CardA, Card4}
            , {CardA, Card5}
            , {CardA, Card6}
            , {CardA, Card7}
            , {CardA, Card8}
            , {CardA, Card9}
            , {Card2, Card2}
            , {Card3, Card3}
            , {Card4, Card4}
            , {Card5, Card5}
            , {Card6, Card6}
            , {Card7, Card7}
            , {Card8, Card8}
            , {Card9, Card9}
            , {Card10, Card10}
            , {CardA, CardA}
        };
        assert(row < 32);
        assert(firstOrSecond == 0 || firstOrSecond == 1);
        return rowToHand[row][firstOrSecond];
    }

    static Card ColumnToDlrCard(size_t col)
    {
        static const Card columnToDlrCard[] =
        {
            Card2, Card3, Card4, Card5, Card6, Card7, Card8, Card9, Card10, CardA
        };
        assert(col < 10);
        return columnToDlrCard[col];
    }

public:
    ResultsTable(QWidget* qw);

    void Fill(const GameActionsLib& lib);
    void Clear();

    static const QColor& GameActionToColor(GameAction::GameAction dec)
    {
        static const QColor decisionToColor[] =
        {
            QColor(0, 120, 190)
            , QColor(0, 200, 0)
            , QColor(70, 240, 0)
            , QColor(50, 200, 200)
            , QColor(240, 225, 0)
            , QColor(255, 255, 255)

            //Lady light coloring
//            QColor(230, 230, 255)
//            , QColor(230, 255, 230)
//            , QColor(255, 255, 200)
//            , QColor(200, 255, 255)
//            , QColor(255, 230, 230)
//            , QColor(255, 255, 255)
        };
        assert(dec >= GameAction::GAStand && dec <= GameAction::GALast);
        return decisionToColor[dec];
    }
};

#endif // RESULTSTABLE_H
