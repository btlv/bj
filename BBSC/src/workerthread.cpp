#include "workerthread.h"
#include "../../BJEngine/src/Core.h"

WorkerThread::WorkerThread(const Rules& rules):
        stopped_(false)
        , calculator_(Core::GetCore().NewStrategyCalculator())
{
	calculator_->Reset(rules);
}

void WorkerThread::run()
{
    while(calculator_->BuildNextPiece() && !stopped_)
        emit ProgressChanged((int)(100 * calculator_->BuiltPart()));
    if(!stopped_)
        emit CalculationFinished();
}
