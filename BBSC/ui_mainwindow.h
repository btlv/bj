/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Sat 15. Oct 15:54:02 2011
**      by: Qt User Interface Compiler version 4.7.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QTableWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "resultstable.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSave;
    QAction *actionLoad;
    QAction *actionAbout;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_5;
    QGroupBox *rulesBox;
    QVBoxLayout *verticalLayout;
    QCheckBox *checkBox_DealerHitsS17;
    QCheckBox *checkBox_DealerPeeks;
    QFrame *line;
    QRadioButton *radioButton_DoubleAny;
    QRadioButton *radioButton_Double_9_10_11;
    QRadioButton *radioButton_Double_10_11;
    QFrame *line_2;
    QCheckBox *checkBox_DAS;
    QHBoxLayout *horizontalLayout_2;
    QSpinBox *maxSplits;
    QLabel *labelSplits;
    QCheckBox *checkBox_AcesResplit;
    QFrame *line_3;
    QRadioButton *radioButton_NoSur;
    QRadioButton *radioButton_LateSur;
    QRadioButton *radioButton_EarlySur;
    QGroupBox *otherBox;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkBox_UseDB;
    QPushButton *runButton;
    QPushButton *newButton;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QLabel *label_3;
    ResultsTable *tableWidget;
    QTableWidget *resultsLegend;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuHelp;
    QStatusBar *statusBar;
    QButtonGroup *buttonGroup_2;
    QButtonGroup *buttonGroup;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setWindowModality(Qt::NonModal);
        MainWindow->resize(598, 512);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(598, 512));
        MainWindow->setSizeIncrement(QSize(2, 2));
        MainWindow->setBaseSize(QSize(598, 512));
#ifndef QT_NO_TOOLTIP
        MainWindow->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        MainWindow->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        MainWindow->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        MainWindow->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        MainWindow->setWindowFilePath(QString::fromUtf8(""));
#ifndef QT_NO_ACCESSIBILITY
        MainWindow->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        MainWindow->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionSave->setEnabled(false);
        actionLoad = new QAction(MainWindow);
        actionLoad->setObjectName(QString::fromUtf8("actionLoad"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionAbout->setText(QString::fromUtf8("About"));
        actionAbout->setIconText(QString::fromUtf8("About"));
#ifndef QT_NO_TOOLTIP
        actionAbout->setToolTip(QString::fromUtf8("About"));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionAbout->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        actionAbout->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        actionAbout->setShortcut(QString::fromUtf8(""));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
#ifndef QT_NO_TOOLTIP
        centralWidget->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        centralWidget->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        centralWidget->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        centralWidget->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        centralWidget->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMinimumSize);
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setSizeConstraint(QLayout::SetMinimumSize);
        rulesBox = new QGroupBox(centralWidget);
        rulesBox->setObjectName(QString::fromUtf8("rulesBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(rulesBox->sizePolicy().hasHeightForWidth());
        rulesBox->setSizePolicy(sizePolicy1);
        rulesBox->setMinimumSize(QSize(190, 355));
        rulesBox->setMaximumSize(QSize(16777215, 16777215));
        rulesBox->setSizeIncrement(QSize(1, 1));
        rulesBox->setBaseSize(QSize(190, 355));
        verticalLayout = new QVBoxLayout(rulesBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, 5, -1, 5);
        checkBox_DealerHitsS17 = new QCheckBox(rulesBox);
        checkBox_DealerHitsS17->setObjectName(QString::fromUtf8("checkBox_DealerHitsS17"));
        sizePolicy1.setHeightForWidth(checkBox_DealerHitsS17->sizePolicy().hasHeightForWidth());
        checkBox_DealerHitsS17->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(checkBox_DealerHitsS17);

        checkBox_DealerPeeks = new QCheckBox(rulesBox);
        checkBox_DealerPeeks->setObjectName(QString::fromUtf8("checkBox_DealerPeeks"));
        sizePolicy1.setHeightForWidth(checkBox_DealerPeeks->sizePolicy().hasHeightForWidth());
        checkBox_DealerPeeks->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(checkBox_DealerPeeks);

        line = new QFrame(rulesBox);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        radioButton_DoubleAny = new QRadioButton(rulesBox);
        buttonGroup = new QButtonGroup(MainWindow);
        buttonGroup->setObjectName(QString::fromUtf8("buttonGroup"));
        buttonGroup->addButton(radioButton_DoubleAny);
        radioButton_DoubleAny->setObjectName(QString::fromUtf8("radioButton_DoubleAny"));
        sizePolicy1.setHeightForWidth(radioButton_DoubleAny->sizePolicy().hasHeightForWidth());
        radioButton_DoubleAny->setSizePolicy(sizePolicy1);
        radioButton_DoubleAny->setChecked(true);

        verticalLayout->addWidget(radioButton_DoubleAny);

        radioButton_Double_9_10_11 = new QRadioButton(rulesBox);
        buttonGroup->addButton(radioButton_Double_9_10_11);
        radioButton_Double_9_10_11->setObjectName(QString::fromUtf8("radioButton_Double_9_10_11"));
        sizePolicy1.setHeightForWidth(radioButton_Double_9_10_11->sizePolicy().hasHeightForWidth());
        radioButton_Double_9_10_11->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(8);
        radioButton_Double_9_10_11->setFont(font);
        radioButton_Double_9_10_11->setChecked(false);

        verticalLayout->addWidget(radioButton_Double_9_10_11);

        radioButton_Double_10_11 = new QRadioButton(rulesBox);
        buttonGroup->addButton(radioButton_Double_10_11);
        radioButton_Double_10_11->setObjectName(QString::fromUtf8("radioButton_Double_10_11"));
        sizePolicy1.setHeightForWidth(radioButton_Double_10_11->sizePolicy().hasHeightForWidth());
        radioButton_Double_10_11->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(radioButton_Double_10_11);

        line_2 = new QFrame(rulesBox);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        checkBox_DAS = new QCheckBox(rulesBox);
        checkBox_DAS->setObjectName(QString::fromUtf8("checkBox_DAS"));
        sizePolicy1.setHeightForWidth(checkBox_DAS->sizePolicy().hasHeightForWidth());
        checkBox_DAS->setSizePolicy(sizePolicy1);
        checkBox_DAS->setChecked(true);

        verticalLayout->addWidget(checkBox_DAS);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        maxSplits = new QSpinBox(rulesBox);
        maxSplits->setObjectName(QString::fromUtf8("maxSplits"));
        sizePolicy1.setHeightForWidth(maxSplits->sizePolicy().hasHeightForWidth());
        maxSplits->setSizePolicy(sizePolicy1);
        maxSplits->setMinimumSize(QSize(40, 20));
        maxSplits->setMaximumSize(QSize(60, 30));
        maxSplits->setMouseTracking(true);
#ifndef QT_NO_TOOLTIP
        maxSplits->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        maxSplits->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
        maxSplits->setReadOnly(false);
        maxSplits->setSpecialValueText(QString::fromUtf8(""));
        maxSplits->setSuffix(QString::fromUtf8(""));
        maxSplits->setPrefix(QString::fromUtf8(""));
        maxSplits->setMaximum(5);
        maxSplits->setValue(3);
#ifndef QT_NO_ACCESSIBILITY
        maxSplits->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY

        horizontalLayout_2->addWidget(maxSplits);

        labelSplits = new QLabel(rulesBox);
        labelSplits->setObjectName(QString::fromUtf8("labelSplits"));
        sizePolicy1.setHeightForWidth(labelSplits->sizePolicy().hasHeightForWidth());
        labelSplits->setSizePolicy(sizePolicy1);
        labelSplits->setMinimumSize(QSize(120, 20));
#ifndef QT_NO_TOOLTIP
        labelSplits->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        labelSplits->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        labelSplits->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        labelSplits->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        labelSplits->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY

        horizontalLayout_2->addWidget(labelSplits);


        verticalLayout->addLayout(horizontalLayout_2);

        checkBox_AcesResplit = new QCheckBox(rulesBox);
        checkBox_AcesResplit->setObjectName(QString::fromUtf8("checkBox_AcesResplit"));
        sizePolicy1.setHeightForWidth(checkBox_AcesResplit->sizePolicy().hasHeightForWidth());
        checkBox_AcesResplit->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(checkBox_AcesResplit);

        line_3 = new QFrame(rulesBox);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);

        radioButton_NoSur = new QRadioButton(rulesBox);
        buttonGroup_2 = new QButtonGroup(MainWindow);
        buttonGroup_2->setObjectName(QString::fromUtf8("buttonGroup_2"));
        buttonGroup_2->addButton(radioButton_NoSur);
        radioButton_NoSur->setObjectName(QString::fromUtf8("radioButton_NoSur"));
        sizePolicy1.setHeightForWidth(radioButton_NoSur->sizePolicy().hasHeightForWidth());
        radioButton_NoSur->setSizePolicy(sizePolicy1);
#ifndef QT_NO_TOOLTIP
        radioButton_NoSur->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_NoSur->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_NoSur->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        radioButton_NoSur->setText(QString::fromUtf8("No Surrender"));
        radioButton_NoSur->setChecked(true);
#ifndef QT_NO_ACCESSIBILITY
        radioButton_NoSur->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        radioButton_NoSur->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY

        verticalLayout->addWidget(radioButton_NoSur);

        radioButton_LateSur = new QRadioButton(rulesBox);
        buttonGroup_2->addButton(radioButton_LateSur);
        radioButton_LateSur->setObjectName(QString::fromUtf8("radioButton_LateSur"));
        sizePolicy1.setHeightForWidth(radioButton_LateSur->sizePolicy().hasHeightForWidth());
        radioButton_LateSur->setSizePolicy(sizePolicy1);
#ifndef QT_NO_TOOLTIP
        radioButton_LateSur->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_LateSur->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_LateSur->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        radioButton_LateSur->setText(QString::fromUtf8("Late Surrender"));
#ifndef QT_NO_ACCESSIBILITY
        radioButton_LateSur->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        radioButton_LateSur->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY

        verticalLayout->addWidget(radioButton_LateSur);

        radioButton_EarlySur = new QRadioButton(rulesBox);
        buttonGroup_2->addButton(radioButton_EarlySur);
        radioButton_EarlySur->setObjectName(QString::fromUtf8("radioButton_EarlySur"));
        sizePolicy1.setHeightForWidth(radioButton_EarlySur->sizePolicy().hasHeightForWidth());
        radioButton_EarlySur->setSizePolicy(sizePolicy1);
#ifndef QT_NO_TOOLTIP
        radioButton_EarlySur->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButton_EarlySur->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        radioButton_EarlySur->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        radioButton_EarlySur->setText(QString::fromUtf8("Early Surrender"));
#ifndef QT_NO_ACCESSIBILITY
        radioButton_EarlySur->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        radioButton_EarlySur->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY

        verticalLayout->addWidget(radioButton_EarlySur);


        verticalLayout_5->addWidget(rulesBox);

        otherBox = new QGroupBox(centralWidget);
        otherBox->setObjectName(QString::fromUtf8("otherBox"));
        sizePolicy1.setHeightForWidth(otherBox->sizePolicy().hasHeightForWidth());
        otherBox->setSizePolicy(sizePolicy1);
        otherBox->setMinimumSize(QSize(190, 30));
        otherBox->setMaximumSize(QSize(16777215, 16777215));
        otherBox->setSizeIncrement(QSize(1, 1));
        otherBox->setBaseSize(QSize(190, 30));
#ifndef QT_NO_TOOLTIP
        otherBox->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        otherBox->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        otherBox->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        otherBox->setTitle(QString::fromUtf8(""));
#ifndef QT_NO_ACCESSIBILITY
        otherBox->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        otherBox->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
        verticalLayout_2 = new QVBoxLayout(otherBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, 3, -1, 3);
        checkBox_UseDB = new QCheckBox(otherBox);
        checkBox_UseDB->setObjectName(QString::fromUtf8("checkBox_UseDB"));
        sizePolicy1.setHeightForWidth(checkBox_UseDB->sizePolicy().hasHeightForWidth());
        checkBox_UseDB->setSizePolicy(sizePolicy1);
        checkBox_UseDB->setMinimumSize(QSize(0, 0));

        verticalLayout_2->addWidget(checkBox_UseDB);


        verticalLayout_5->addWidget(otherBox);

        runButton = new QPushButton(centralWidget);
        runButton->setObjectName(QString::fromUtf8("runButton"));
        sizePolicy1.setHeightForWidth(runButton->sizePolicy().hasHeightForWidth());
        runButton->setSizePolicy(sizePolicy1);
        runButton->setMinimumSize(QSize(190, 24));
        runButton->setMaximumSize(QSize(16777215, 16777215));
        runButton->setSizeIncrement(QSize(1, 1));
        runButton->setBaseSize(QSize(169, 24));
#ifndef QT_NO_STATUSTIP
        runButton->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP

        verticalLayout_5->addWidget(runButton);

        newButton = new QPushButton(centralWidget);
        newButton->setObjectName(QString::fromUtf8("newButton"));
        newButton->setEnabled(false);
        sizePolicy1.setHeightForWidth(newButton->sizePolicy().hasHeightForWidth());
        newButton->setSizePolicy(sizePolicy1);
        newButton->setMinimumSize(QSize(190, 24));
        newButton->setMaximumSize(QSize(16777215, 16777215));
        newButton->setSizeIncrement(QSize(1, 1));
        newButton->setBaseSize(QSize(190, 24));
#ifndef QT_NO_TOOLTIP
        newButton->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        newButton->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        newButton->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        newButton->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        newButton->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY

        verticalLayout_5->addWidget(newButton);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_2);


        horizontalLayout->addLayout(verticalLayout_5);

        horizontalSpacer = new QSpacerItem(10, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        gridLayout_2->setHorizontalSpacing(2);
        gridLayout_2->setVerticalSpacing(3);
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);
        label->setMinimumSize(QSize(360, 14));
        label->setSizeIncrement(QSize(1, 1));
        label->setBaseSize(QSize(360, 14));
        label->setFrameShape(QFrame::NoFrame);
        label->setScaledContents(false);
        label->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label, 0, 1, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy3);
        label_3->setMinimumSize(QSize(14, 351));
#ifndef QT_NO_TOOLTIP
        label_3->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        label_3->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        label_3->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        label_3->setTextFormat(Qt::RichText);
        label_3->setAlignment(Qt::AlignCenter);
#ifndef QT_NO_ACCESSIBILITY
        label_3->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        label_3->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY

        gridLayout_2->addWidget(label_3, 1, 0, 1, 1);

        tableWidget = new ResultsTable(centralWidget);
        if (tableWidget->columnCount() < 10)
            tableWidget->setColumnCount(10);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        if (tableWidget->rowCount() < 32)
            tableWidget->setRowCount(32);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(1, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(2, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(3, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(4, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(5, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(6, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(7, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(8, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(9, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(10, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(11, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(12, __qtablewidgetitem22);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(13, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(14, __qtablewidgetitem24);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(15, __qtablewidgetitem25);
        QTableWidgetItem *__qtablewidgetitem26 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(16, __qtablewidgetitem26);
        QTableWidgetItem *__qtablewidgetitem27 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(17, __qtablewidgetitem27);
        QTableWidgetItem *__qtablewidgetitem28 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(18, __qtablewidgetitem28);
        QTableWidgetItem *__qtablewidgetitem29 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(19, __qtablewidgetitem29);
        QTableWidgetItem *__qtablewidgetitem30 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(20, __qtablewidgetitem30);
        QTableWidgetItem *__qtablewidgetitem31 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(21, __qtablewidgetitem31);
        QTableWidgetItem *__qtablewidgetitem32 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(22, __qtablewidgetitem32);
        QTableWidgetItem *__qtablewidgetitem33 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(23, __qtablewidgetitem33);
        QTableWidgetItem *__qtablewidgetitem34 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(24, __qtablewidgetitem34);
        QTableWidgetItem *__qtablewidgetitem35 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(25, __qtablewidgetitem35);
        QTableWidgetItem *__qtablewidgetitem36 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(26, __qtablewidgetitem36);
        QTableWidgetItem *__qtablewidgetitem37 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(27, __qtablewidgetitem37);
        QTableWidgetItem *__qtablewidgetitem38 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(28, __qtablewidgetitem38);
        QTableWidgetItem *__qtablewidgetitem39 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(29, __qtablewidgetitem39);
        QTableWidgetItem *__qtablewidgetitem40 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(30, __qtablewidgetitem40);
        QTableWidgetItem *__qtablewidgetitem41 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(31, __qtablewidgetitem41);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
        tableWidget->setSizePolicy(sizePolicy4);
        tableWidget->setMinimumSize(QSize(360, 410));
        tableWidget->setSizeIncrement(QSize(1, 1));
        tableWidget->setBaseSize(QSize(360, 410));
        tableWidget->setMouseTracking(true);
#ifndef QT_NO_TOOLTIP
        tableWidget->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        tableWidget->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        tableWidget->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setProperty("showDropIndicator", QVariant(false));
        tableWidget->setDragDropOverwriteMode(false);
        tableWidget->setCornerButtonEnabled(false);
#ifndef QT_NO_ACCESSIBILITY
        tableWidget->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        tableWidget->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
        tableWidget->horizontalHeader()->setMinimumSectionSize(20);
        tableWidget->verticalHeader()->setDefaultSectionSize(25);
        tableWidget->verticalHeader()->setMinimumSectionSize(25);

        gridLayout_2->addWidget(tableWidget, 1, 1, 1, 1);

        resultsLegend = new QTableWidget(centralWidget);
        if (resultsLegend->columnCount() < 5)
            resultsLegend->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem42 = new QTableWidgetItem();
        __qtablewidgetitem42->setText(QString::fromUtf8("1"));
        resultsLegend->setHorizontalHeaderItem(0, __qtablewidgetitem42);
        QTableWidgetItem *__qtablewidgetitem43 = new QTableWidgetItem();
        resultsLegend->setHorizontalHeaderItem(1, __qtablewidgetitem43);
        QTableWidgetItem *__qtablewidgetitem44 = new QTableWidgetItem();
        resultsLegend->setHorizontalHeaderItem(2, __qtablewidgetitem44);
        QTableWidgetItem *__qtablewidgetitem45 = new QTableWidgetItem();
        resultsLegend->setHorizontalHeaderItem(3, __qtablewidgetitem45);
        QTableWidgetItem *__qtablewidgetitem46 = new QTableWidgetItem();
        resultsLegend->setHorizontalHeaderItem(4, __qtablewidgetitem46);
        if (resultsLegend->rowCount() < 1)
            resultsLegend->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem47 = new QTableWidgetItem();
        resultsLegend->setVerticalHeaderItem(0, __qtablewidgetitem47);
        QTableWidgetItem *__qtablewidgetitem48 = new QTableWidgetItem();
        __qtablewidgetitem48->setText(QString::fromUtf8("S: Stand"));
        __qtablewidgetitem48->setFlags(Qt::ItemIsEnabled);
        resultsLegend->setItem(0, 0, __qtablewidgetitem48);
        QTableWidgetItem *__qtablewidgetitem49 = new QTableWidgetItem();
        __qtablewidgetitem49->setText(QString::fromUtf8("H: Hit"));
        __qtablewidgetitem49->setFlags(Qt::ItemIsEnabled);
        resultsLegend->setItem(0, 1, __qtablewidgetitem49);
        QTableWidgetItem *__qtablewidgetitem50 = new QTableWidgetItem();
        __qtablewidgetitem50->setText(QString::fromUtf8("D: Double"));
        __qtablewidgetitem50->setFlags(Qt::ItemIsEnabled);
        resultsLegend->setItem(0, 2, __qtablewidgetitem50);
        QTableWidgetItem *__qtablewidgetitem51 = new QTableWidgetItem();
        __qtablewidgetitem51->setText(QString::fromUtf8("P: Split"));
        __qtablewidgetitem51->setFlags(Qt::ItemIsEnabled);
        resultsLegend->setItem(0, 3, __qtablewidgetitem51);
        QTableWidgetItem *__qtablewidgetitem52 = new QTableWidgetItem();
        __qtablewidgetitem52->setText(QString::fromUtf8("R: Surrender"));
        __qtablewidgetitem52->setFlags(Qt::ItemIsEnabled);
        resultsLegend->setItem(0, 4, __qtablewidgetitem52);
        resultsLegend->setObjectName(QString::fromUtf8("resultsLegend"));
        resultsLegend->setEnabled(true);
        sizePolicy2.setHeightForWidth(resultsLegend->sizePolicy().hasHeightForWidth());
        resultsLegend->setSizePolicy(sizePolicy2);
        resultsLegend->setMinimumSize(QSize(360, 21));
        resultsLegend->setMaximumSize(QSize(16777215, 32));
        resultsLegend->setSizeIncrement(QSize(1, 1));
        resultsLegend->setBaseSize(QSize(360, 21));
#ifndef QT_NO_TOOLTIP
        resultsLegend->setToolTip(QString::fromUtf8(""));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        resultsLegend->setStatusTip(QString::fromUtf8(""));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_WHATSTHIS
        resultsLegend->setWhatsThis(QString::fromUtf8(""));
#endif // QT_NO_WHATSTHIS
        resultsLegend->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        resultsLegend->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        resultsLegend->setAutoScroll(false);
        resultsLegend->setEditTriggers(QAbstractItemView::NoEditTriggers);
        resultsLegend->setTabKeyNavigation(false);
        resultsLegend->setCornerButtonEnabled(false);
#ifndef QT_NO_ACCESSIBILITY
        resultsLegend->setProperty("accessibleName", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        resultsLegend->setProperty("accessibleDescription", QVariant(QString::fromUtf8("")));
#endif // QT_NO_ACCESSIBILITY
        resultsLegend->horizontalHeader()->setVisible(false);
        resultsLegend->horizontalHeader()->setCascadingSectionResizes(false);
        resultsLegend->horizontalHeader()->setHighlightSections(true);
        resultsLegend->horizontalHeader()->setMinimumSectionSize(35);
        resultsLegend->horizontalHeader()->setStretchLastSection(false);
        resultsLegend->verticalHeader()->setVisible(false);
        resultsLegend->verticalHeader()->setDefaultSectionSize(21);
        resultsLegend->verticalHeader()->setHighlightSections(false);
        resultsLegend->verticalHeader()->setMinimumSectionSize(21);

        gridLayout_2->addWidget(resultsLegend, 2, 1, 1, 1);


        horizontalLayout->addLayout(gridLayout_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 598, 18));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuFile->setTitle(QString::fromUtf8("File"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(radioButton_NoSur, radioButton_EarlySur);
        QWidget::setTabOrder(radioButton_EarlySur, checkBox_DAS);
        QWidget::setTabOrder(checkBox_DAS, radioButton_DoubleAny);
        QWidget::setTabOrder(radioButton_DoubleAny, radioButton_Double_9_10_11);
        QWidget::setTabOrder(radioButton_Double_9_10_11, radioButton_Double_10_11);
        QWidget::setTabOrder(radioButton_Double_10_11, checkBox_DealerHitsS17);
        QWidget::setTabOrder(checkBox_DealerHitsS17, tableWidget);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionSave);
        menuFile->addAction(actionLoad);
        menuHelp->addAction(actionAbout);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "BJ Basic Strategy Calculator", 0, QApplication::UnicodeUTF8));
        actionSave->setText(QApplication::translate("MainWindow", "Save Strategy", 0, QApplication::UnicodeUTF8));
        actionLoad->setText(QApplication::translate("MainWindow", "Load Strategy", 0, QApplication::UnicodeUTF8));
        rulesBox->setTitle(QApplication::translate("MainWindow", "Rules", 0, QApplication::UnicodeUTF8));
        checkBox_DealerHitsS17->setText(QApplication::translate("MainWindow", "Dealer Hits Soft 17", 0, QApplication::UnicodeUTF8));
        checkBox_DealerPeeks->setText(QApplication::translate("MainWindow", "Dealer Peeks for BJ", 0, QApplication::UnicodeUTF8));
        radioButton_DoubleAny->setText(QApplication::translate("MainWindow", "Double Any 2 Cards", 0, QApplication::UnicodeUTF8));
        radioButton_Double_9_10_11->setText(QApplication::translate("MainWindow", "Double 9, 10, 11 Only", 0, QApplication::UnicodeUTF8));
        radioButton_Double_10_11->setText(QApplication::translate("MainWindow", "Double 10, 11 Only", 0, QApplication::UnicodeUTF8));
        checkBox_DAS->setText(QApplication::translate("MainWindow", "Double After Split", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        maxSplits->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_ACCESSIBILITY
        maxSplits->setProperty("accessibleDescription", QVariant(QString()));
#endif // QT_NO_ACCESSIBILITY
        labelSplits->setText(QApplication::translate("MainWindow", "Max Splits", 0, QApplication::UnicodeUTF8));
        checkBox_AcesResplit->setText(QApplication::translate("MainWindow", "Aces Resplit", 0, QApplication::UnicodeUTF8));
        checkBox_UseDB->setText(QApplication::translate("MainWindow", "Use Database", 0, QApplication::UnicodeUTF8));
        runButton->setText(QApplication::translate("MainWindow", "Get the Strategy", 0, QApplication::UnicodeUTF8));
        newButton->setText(QApplication::translate("MainWindow", "Change Rules", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Dealer Up Card", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Y</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">o</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">u</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">r</span></p>\n"
"<"
                        "p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">H</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">a</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">n</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">d</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "2", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "3", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "4", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "5", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("MainWindow", "6", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("MainWindow", "7", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("MainWindow", "8", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("MainWindow", "9", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QApplication::translate("MainWindow", "T", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QApplication::translate("MainWindow", "A", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->verticalHeaderItem(0);
        ___qtablewidgetitem10->setText(QApplication::translate("MainWindow", "5", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget->verticalHeaderItem(1);
        ___qtablewidgetitem11->setText(QApplication::translate("MainWindow", "6", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget->verticalHeaderItem(2);
        ___qtablewidgetitem12->setText(QApplication::translate("MainWindow", "7", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem13 = tableWidget->verticalHeaderItem(3);
        ___qtablewidgetitem13->setText(QApplication::translate("MainWindow", "8", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem14 = tableWidget->verticalHeaderItem(4);
        ___qtablewidgetitem14->setText(QApplication::translate("MainWindow", "9", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget->verticalHeaderItem(5);
        ___qtablewidgetitem15->setText(QApplication::translate("MainWindow", "10", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem16 = tableWidget->verticalHeaderItem(6);
        ___qtablewidgetitem16->setText(QApplication::translate("MainWindow", "11", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem17 = tableWidget->verticalHeaderItem(7);
        ___qtablewidgetitem17->setText(QApplication::translate("MainWindow", "12", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem18 = tableWidget->verticalHeaderItem(8);
        ___qtablewidgetitem18->setText(QApplication::translate("MainWindow", "13", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem19 = tableWidget->verticalHeaderItem(9);
        ___qtablewidgetitem19->setText(QApplication::translate("MainWindow", "14", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem20 = tableWidget->verticalHeaderItem(10);
        ___qtablewidgetitem20->setText(QApplication::translate("MainWindow", "15", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem21 = tableWidget->verticalHeaderItem(11);
        ___qtablewidgetitem21->setText(QApplication::translate("MainWindow", "16", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem22 = tableWidget->verticalHeaderItem(12);
        ___qtablewidgetitem22->setText(QApplication::translate("MainWindow", "17", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem23 = tableWidget->verticalHeaderItem(13);
        ___qtablewidgetitem23->setText(QApplication::translate("MainWindow", "18", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem24 = tableWidget->verticalHeaderItem(14);
        ___qtablewidgetitem24->setText(QApplication::translate("MainWindow", "S13", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem25 = tableWidget->verticalHeaderItem(15);
        ___qtablewidgetitem25->setText(QApplication::translate("MainWindow", "S14", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem26 = tableWidget->verticalHeaderItem(16);
        ___qtablewidgetitem26->setText(QApplication::translate("MainWindow", "S15", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem27 = tableWidget->verticalHeaderItem(17);
        ___qtablewidgetitem27->setText(QApplication::translate("MainWindow", "S16", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem28 = tableWidget->verticalHeaderItem(18);
        ___qtablewidgetitem28->setText(QApplication::translate("MainWindow", "S17", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem29 = tableWidget->verticalHeaderItem(19);
        ___qtablewidgetitem29->setText(QApplication::translate("MainWindow", "S18", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem30 = tableWidget->verticalHeaderItem(20);
        ___qtablewidgetitem30->setText(QApplication::translate("MainWindow", "S19", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem31 = tableWidget->verticalHeaderItem(21);
        ___qtablewidgetitem31->setText(QApplication::translate("MainWindow", "S20", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem32 = tableWidget->verticalHeaderItem(22);
        ___qtablewidgetitem32->setText(QApplication::translate("MainWindow", "2, 2", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem33 = tableWidget->verticalHeaderItem(23);
        ___qtablewidgetitem33->setText(QApplication::translate("MainWindow", "3, 3", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem34 = tableWidget->verticalHeaderItem(24);
        ___qtablewidgetitem34->setText(QApplication::translate("MainWindow", "4, 4", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem35 = tableWidget->verticalHeaderItem(25);
        ___qtablewidgetitem35->setText(QApplication::translate("MainWindow", "5, 5", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem36 = tableWidget->verticalHeaderItem(26);
        ___qtablewidgetitem36->setText(QApplication::translate("MainWindow", "6, 6", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem37 = tableWidget->verticalHeaderItem(27);
        ___qtablewidgetitem37->setText(QApplication::translate("MainWindow", "7, 7", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem38 = tableWidget->verticalHeaderItem(28);
        ___qtablewidgetitem38->setText(QApplication::translate("MainWindow", "8, 8", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem39 = tableWidget->verticalHeaderItem(29);
        ___qtablewidgetitem39->setText(QApplication::translate("MainWindow", "9, 9", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem40 = tableWidget->verticalHeaderItem(30);
        ___qtablewidgetitem40->setText(QApplication::translate("MainWindow", "T, T", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem41 = tableWidget->verticalHeaderItem(31);
        ___qtablewidgetitem41->setText(QApplication::translate("MainWindow", "A, A", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem42 = resultsLegend->horizontalHeaderItem(1);
        ___qtablewidgetitem42->setText(QApplication::translate("MainWindow", "2", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem43 = resultsLegend->horizontalHeaderItem(2);
        ___qtablewidgetitem43->setText(QApplication::translate("MainWindow", "4", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem44 = resultsLegend->horizontalHeaderItem(3);
        ___qtablewidgetitem44->setText(QApplication::translate("MainWindow", "3", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem45 = resultsLegend->horizontalHeaderItem(4);
        ___qtablewidgetitem45->setText(QApplication::translate("MainWindow", "5", 0, QApplication::UnicodeUTF8));
        QTableWidgetItem *___qtablewidgetitem46 = resultsLegend->verticalHeaderItem(0);
        ___qtablewidgetitem46->setText(QApplication::translate("MainWindow", "0", 0, QApplication::UnicodeUTF8));

        const bool __sortingEnabled = resultsLegend->isSortingEnabled();
        resultsLegend->setSortingEnabled(false);
        resultsLegend->setSortingEnabled(__sortingEnabled);

        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
