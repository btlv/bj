#ifndef _TASK_H_
#define _TASK_H_

#include "Action.h"
#include "Containers.h"

#include "ThreadPool\threadpool.hpp"

struct Rules;
class  Box;

class ConsiderSituationTask
{
private:
    const SituationDesc& desc_;
    GameActionsLib&      lib_;
    const Rules&         rules_;
    boost::mutex&        mtx_;
    const size_t         iterationsCount_;

    const GameActionProfit& GetBestLegalAction(const Box& box, size_t j, const SituationDesc& desc) const;

    void (ConsiderSituationTask::*ActionPerformers[5])(IDeck&, GameActionProfit&);
    void Hit(IDeck& deck, GameActionProfit& dp);
    void StandAndDouble(IDeck& deck, GameActionProfit& dp, GameAction::GameAction d);
    void Split(IDeck& deck, GameActionProfit& dp);
    void Surrender(IDeck& deck, GameActionProfit& dp);
public:
    ConsiderSituationTask(const SituationDesc& desc, GameActionsLib& decisionsLib, const Rules& rules, boost::mutex& mtx, size_t iterationsCount = 500000)
        : desc_(desc)
        , lib_(decisionsLib)
        , rules_(rules)
        , mtx_(mtx)
        , iterationsCount_(iterationsCount)
      {}

      void operator()();
};

#endif
