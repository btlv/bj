#include "InfiniteDeck.h"

#include <cstdlib>
#include <ctime>

InfiniteDeck::InfiniteDeck():
        randMult_(((float)(CardLast - Card2)) / RAND_MAX)
{
    srand((unsigned)time(0));
}

Card InfiniteDeck::GetCard()
{
    return (Card)(Card2 + (int)(randMult_ * (float)rand()));
}
