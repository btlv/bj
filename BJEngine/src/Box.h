#ifndef _BOX_H_
#define _BOX_H_

#include "Hand.h"

struct Rules;

class Box
{
private:
    std::vector<PlaingHand> hands_;
    unsigned int            splitsDone_;
    bool                    initialAcesOnly_;
public:
    Box(Card card1, Card card2);
    Box(const Hand& hand);

    void Surrender();

    void Double(size_t i, IDeck& deck);

    void Split(size_t i);

    //Box must consist of only one hand, which must be a pair
    void SplitUntilPossible(IDeck& deck, const Rules& rules);

    void Hit(size_t i, IDeck& deck);

    void CompleteHand(size_t i, IDeck& deck, unsigned int hardScore, unsigned int softScore);

    size_t HandsCount() const {return hands_.size();}

    const Hand& GetHand(size_t i) const {return hands_[i];}

    unsigned int SplitsDone() const {return splitsDone_;}

    bool WasPairOfAces() const {return initialAcesOnly_;}

    float TakeProfit(const Hand& dealerHand) const;
};

#endif
