#ifndef _COMMON_H_
#define _COMMON_H_

#ifndef min
#define min(x,y) x < y ? x : y
#endif

#ifndef max
#define max(x,y) x > y ? x : y
#endif

#endif // COMMON_H
