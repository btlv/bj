#include "Core.h"
#include "InfiniteDeck.h"
#include "StrategyCalculator.h"

IDeck& Core::GetDeck()
{
    return *deck_;
}

IStrategyCalculatorPtr Core::NewStrategyCalculator()
{
	Rules rules;
	return IStrategyCalculatorPtr (new StrategyCalculator(rules));
}

std::ofstream& Core::GetLog()
{
	return log_;
}

Core& Core::GetCore()
{
	//no need in
	//static Core* core = new Core();
	//things aren't that complex yet
    static Core core;
    return core;
}

Core::Core(const Core& core){}

Core::~Core()
{
	delete deck_;

	log_.flush();
	log_.close();
}

Core::Core():
deck_(new InfiniteDeck())
, log_("bjengine.log", std::ios::out)
{
	log_ << "Core was successfully created" << std::endl;
}
