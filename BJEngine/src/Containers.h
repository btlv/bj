#pragma once
//#ifndef _CONTAINERS_H_
//#define _CONTAINERS_H_

#include "Action.h"
#include "stdafx.h"

#include <map>
#include <set>
#include <vector>
#include <fstream>

class GameActionsTable: public std::set<GameActionProfit>
{
public:
    void Serialize(std::ostream& os) const;
    void UnSerialize(std::istream& is);
};

//hash_map isn't faster in this case
class GameActionsLib: public std::map<SituationDesc, GameActionsTable>
{
public:
    void BJ_EXPORT Serialize(std::ostream& os) const;
    void BJ_EXPORT UnSerialize(std::istream& is);
};

//#endif
