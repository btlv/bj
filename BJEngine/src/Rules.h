#ifndef _RULES_H_
#define _RULES_H_

#include "Box.h"
#include "Action.h"

#include "stdafx.h"

#include <fstream>

struct BJ_EXPORT Rules 
{
    enum Double
    {
        DAny = 0,
        DOnly_9_10_11,
        DOnly_10_11,
        DLast
    };
    enum Surrender
    {
        SNo = 0,
        SLate,
        SEarly,
        SLast
    };

    Double       dbl;
    unsigned int maxSplits;
    Surrender    surr;
    bool         dealerHitsS17;
    bool         dealerPeeks;
    bool         dblAfterSplitAllowed;
    bool         acesResplitAllowed;

    Rules();

    bool IsAllowed(const Box& box, size_t handNum, GameAction::GameAction decision) const;

    void Serialize(std::ostream& os) const;
    void UnSerialize(std::istream& is);
};

#endif
