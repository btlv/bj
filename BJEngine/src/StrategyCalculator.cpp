#include <windows.h>//only for GetSystemInfo

#include "StrategyCalculator.h"
#include "Rules.h"
#include "common.h"

StrategyCalculator::StrategyCalculator(const Rules& rules, size_t maxThreads)
{
    Reset(rules, maxThreads);
}

StrategyCalculator::~StrategyCalculator()
{
    threadPool_.size_controller().resize(0);
}

void StrategyCalculator::Reset(const Rules& rules, size_t maxThreads)
{
    rules_ = rules;
    finishedCount_ = 0;
    lib_.erase(lib_.begin(), lib_.end());//clearing this way to avoid redundant memory reallocations

    static const size_t MaxWorkerThreads = 10;//There can be only 10 different dealer up cards, it's the max parallelism for current algorithm
    SYSTEM_INFO si = {0, };
    GetSystemInfo(&si);
    size_t threadsCount = min(min(si.dwNumberOfProcessors, maxThreads), MaxWorkerThreads);
    if(!threadsCount) threadsCount = 1;

    threadPool_.size_controller().resize(threadsCount);
}

bool StrategyCalculator::BuildNextPiece()
{
    if(finishedCount_ < AllSituations::SituationsCount)
    {
        for(size_t j = 0; j < AllSituations::IndependentSituationsLayerCount; ++j)
            threadPool_.schedule(ConsiderSituationTask(situations_.situations[finishedCount_ + j], lib_, rules_, sync_, 150000));
        threadPool_.wait();
        finishedCount_ += AllSituations::IndependentSituationsLayerCount;
        return true;
    }
    return false;
}

void StrategyCalculator::Set(const GameActionsLib& lib, const Rules& rules)
{
    boost::mutex::scoped_lock lock(sync_);//very unlikely that some other thread may change the lib during the operation, but just in case
    lib_ = lib;
    finishedCount_ = lib_.size();
    rules_ = rules;
}
