#include "Action.h"
#include <assert.h>

void SituationDesc::Set(Card plCard, Card dlrUpCard)
{
    Reset();
    AddCard(plCard);

    Score dlrScore;
    dlrScore.AddCard(dlrUpCard);
    dlrCardScore_ = dlrScore.score;
}

void SituationDesc::Set(Card plCard1, Card plCard2, Card dlrUpCard)
{
    Set(plCard1, dlrUpCard);
    AddCard(plCard2);
}

SituationDesc::SituationDesc(Card plCard1, Card plCard2, Card dlrUpCard)
{
    Set(plCard1, plCard2, dlrUpCard);
}

SituationDesc::SituationDesc(const Hand& hand, unsigned int dlrCardScore):
        Hand(hand)
        , dlrCardScore_(dlrCardScore)
{
}

SituationDesc::SituationDesc()
{

}

void SituationDesc::Serialize(std::ostream& os) const
{
    Hand::Serialize(os);
    os.write((const char*)&dlrCardScore_, sizeof(dlrCardScore_));
}

void SituationDesc::UnSerialize(std::istream& is)
{
    Hand::UnSerialize(is);
    is.read((char*)&dlrCardScore_, sizeof(dlrCardScore_));
}

AllSituations::AllSituations()
{
    static const Card actualCards[] =
    {
        Card2,
        Card3,
        Card4,
        Card5,
        Card6,
        Card7,
        Card8,
        Card9,
        Card10,
        CardA
    };

    size_t descNum = 0;
    for (unsigned int i = Card3; i > CardFirst; --i)
        for (size_t j = 0; j < 10; ++j, ++descNum)
        {
            situations[descNum].Set(Card10, Card8, actualCards[j]);
            situations[descNum].AddCard((Card)i);
        };

    for (unsigned int i = Card9; i > CardFirst; --i)
        for (size_t j = 0; j < 10; ++j, ++descNum)
            situations[descNum].Set(Card10, (Card)i, actualCards[j]);

    for (unsigned int i = Card10; i > CardFirst; --i)
        for (size_t j = 0; j < 10; ++j, ++descNum)
            situations[descNum].Set(CardA, (Card)i, actualCards[j]);

    for (size_t j = 0; j < 10; ++j, ++descNum)
    {
        situations[descNum].Set(CardA, CardA, actualCards[j]);
        situations[descNum].ForceUnPair();
    }

    for (size_t j = 0; j < 10; ++j, ++descNum)
    {
        situations[descNum].Set(CardA, actualCards[j]);
        situations[descNum].ForceUnPair();//Can get another ace when hitting, but we must first consider such situation without splitting
    }

    for (unsigned int i = Card5; i > CardFirst; --i)
        for (size_t j = 0; j < 10; ++j, ++descNum)
            situations[descNum].Set(Card6, (Card)i, actualCards[j]);

    for (size_t i = 0; i < 10; ++i)
        for (size_t j = 0; j < 10; ++j, ++descNum)
            situations[descNum].Set(actualCards[i], actualCards[i], actualCards[j]);

    for (unsigned int i = Card3; i < Card6; ++i)
        for (size_t j = 0; j < 10; ++j, ++descNum)
            situations[descNum].Set(Card2, (Card)i, actualCards[j]);
}

GameActionProfit::GameActionProfit(GameAction::GameAction a):
        action(a)
        , profitBJUnknown_(0)
        , profitNoBJ_(0)
        , bjUnknownTimes_(0)
        , noBJTimes_(0)
{
}

GameActionProfit::GameActionProfit():
        action(GameAction::GALast)
        , profitBJUnknown_(0)
        , profitNoBJ_(0)
        , bjUnknownTimes_(0)
        , noBJTimes_(0)
{
}

void GameActionProfit::AddBJUnknownProfit(float prof)
{
    ++bjUnknownTimes_;
    profitBJUnknown_ += prof;
}

void GameActionProfit::AddNoBJProfit(float prof)
{
    ++noBJTimes_;
    profitNoBJ_ += prof;
    AddBJUnknownProfit(prof);
}

void GameActionProfit::AddProfit(bool dlrPeeks, float prof)
{
    if(dlrPeeks)
        AddNoBJProfit(prof);
    else
        AddBJUnknownProfit(prof);
}

void GameActionProfit::AddingFinished()
{
    assert(bjUnknownTimes_);
    profitBJUnknown_ /= bjUnknownTimes_;
    if(noBJTimes_)
        profitNoBJ_ /= noBJTimes_;
}

float GameActionProfit::GetBJUnknownProfit() const
{
    return (float)profitBJUnknown_;
}

float GameActionProfit::GetNoBJProfit(bool dlrPeeks) const
{
    if(dlrPeeks)
        return (float)profitNoBJ_;
    else
        return (float)profitBJUnknown_;
}

bool GameActionProfit::operator < (const GameActionProfit& other) const
{
    if(profitBJUnknown_ != other.profitBJUnknown_)
        return profitBJUnknown_ > other.profitBJUnknown_;
    else
    {
        if(profitNoBJ_ != other.profitNoBJ_)
            return profitNoBJ_ > other.profitNoBJ_;
        else
            return action > other.action;
    }
}
