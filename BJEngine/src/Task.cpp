#include "Task.h"
#include "Core.h"
#include "Rules.h"

#include <assert.h>

static bool DealerHasBJ(const Hand& dlrHand, const Box& box, const Rules& rules, GameActionProfit& dp)
{
    assert(box.HandsCount() == 1);
    if(rules.dealerPeeks && dlrHand.IsBJ())
    {
        dp.AddBJUnknownProfit(box.TakeProfit(dlrHand));
        return true;
    }
    return false;
}

const GameActionProfit& ConsiderSituationTask::GetBestLegalAction(const Box& box, size_t j, const SituationDesc& desc) const
{
    GameActionsLib::const_iterator it, end;
    {
        boost::mutex::scoped_lock sLock(mtx_);
        it = lib_.find(desc);
        end = lib_.end();
    }

    assert(it != end && !it->second.empty());
    GameActionsTable::const_iterator dIt = it->second.begin();
    while(!rules_.IsAllowed(box, j, dIt->action))
        ++dIt;
    return *dIt;
}

void ConsiderSituationTask::Hit(IDeck& deck, GameActionProfit& dp)
{
    const Score& initialScore  = desc_.GetScore();
    const unsigned int dlrSoftStop = 17 + rules_.dealerHitsS17;
    for(unsigned int i = 0; i < iterationsCount_; ++i)
    {
        Box box(desc_);
        Hand dlrHand(Score::Score2Card(desc_.GetDlrScore()), deck.GetCard());
        if(DealerHasBJ(dlrHand, box, rules_, dp))
            continue;

        if(initialScore.score < 12)
            box.CompleteHand(0, deck, 12, 12);
        else
            box.Hit(0, deck);
        if(box.GetHand(0).GetScore().score < 21)
        {
            SituationDesc newDesc(box.GetHand(0), desc_.GetDlrScore());
            dp.AddProfit(rules_.dealerPeeks, GetBestLegalAction(box, 0, newDesc).GetNoBJProfit(rules_.dealerPeeks));
        }
        else//score == 21, dealer may also get 21 || score > 21 in which case dealer wins (TakeProfit will take loss in that case)
        {
            dlrHand.CompleteHand(deck, 17, dlrSoftStop);
            dp.AddProfit(rules_.dealerPeeks, box.TakeProfit(dlrHand));
        }
    }
}

void ConsiderSituationTask::StandAndDouble(IDeck& deck, GameActionProfit& dp, GameAction::GameAction d)
{
    const unsigned int dlrSoftStop = 17 + rules_.dealerHitsS17;
    for(unsigned int i = 0; i < iterationsCount_; ++i)
    {
        Box box(desc_);
        Hand dlrHand(Score::Score2Card(desc_.GetDlrScore()), deck.GetCard());
        if(DealerHasBJ(dlrHand, box, rules_, dp))
            continue;

        dlrHand.CompleteHand(deck, 17, dlrSoftStop);
        if(d == GameAction::GADouble)
            box.Double(0, deck);
        dp.AddProfit(rules_.dealerPeeks, box.TakeProfit(dlrHand));
    }
}

void ConsiderSituationTask::Split(IDeck& deck, GameActionProfit& dp)
{
    for(unsigned int i = 0; i < iterationsCount_; ++i)
    {
        Box box(desc_);
        Hand dlrHand(Score::Score2Card(desc_.GetDlrScore()), deck.GetCard());
        if(DealerHasBJ(dlrHand, box, rules_, dp))
            continue;

        box.SplitUntilPossible(deck, rules_);
        float profit = 0;
        for (size_t j = 0, size = box.HandsCount(); j < size; ++j)
        {
            box.CompleteHand(j, deck, 9, 12);//12 so that split pairs of aces do not get additional cards, and not to bust
            SituationDesc newDesc(box.GetHand(j), desc_.GetDlrScore());
            newDesc.ForceUnPair();

            profit += GetBestLegalAction(box, j, newDesc).GetNoBJProfit(rules_.dealerPeeks);
        }
        dp.AddProfit(rules_.dealerPeeks, profit);
    }
}

void ConsiderSituationTask::Surrender(IDeck& deck, GameActionProfit& dp)
{
    if(rules_.surr == Rules::SEarly)
    {
        Box box(desc_);
        box.Surrender();
        Hand dlrHand(Score::Score2Card(desc_.GetDlrScore()), deck.GetCard());
        dp.AddBJUnknownProfit(box.TakeProfit(dlrHand));
    }
    else//Rules::SLate
    {
        for(unsigned int i = 0; i < iterationsCount_; ++i)
        {
            Box box(desc_);
            Hand dlrHand(Score::Score2Card(desc_.GetDlrScore()), deck.GetCard());
            if(DealerHasBJ(dlrHand, box, rules_, dp))
                continue;
            box.Surrender();
            dp.AddProfit(rules_.dealerPeeks, box.TakeProfit(dlrHand));
        }
    }
}

void ConsiderSituationTask::operator()()
{
    GameActionsTable results;
    IDeck& deck = Core::GetCore().GetDeck();
    for(unsigned int d = GameAction::GAStand; d < GameAction::GALast; ++d)
    {
        if(d == GameAction::GASplit && CardLast == desc_.IsPair())
            continue;
        if(!rules_.IsAllowed(Box(desc_), 0, (GameAction::GameAction)d))
            continue;

        GameActionProfit dp((GameAction::GameAction)d);

        if(GameAction::GAHit == d)
            Hit(deck, dp);
        else if(d == GameAction::GADouble || d == GameAction::GAStand)
            StandAndDouble(deck, dp, (GameAction::GameAction)d);
        else if(d == GameAction::GASplit)
            Split(deck, dp);
        else if(d == GameAction::GASurrender)
            Surrender(deck, dp);
        dp.AddingFinished();
        results.insert(dp);
    }

    boost::mutex::scoped_lock sLock(mtx_);
    lib_.insert(std::make_pair(desc_, results));
}
