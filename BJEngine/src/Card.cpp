#include "Card.h"

void Score::AddCard(Card card)
{
    score += CardsScores(card);
    if(card == CardA)
    {
        if(score > 21)
            score -= 10;
        else
            isSoft = true;
    }
    if(score > 21 && isSoft)
    {
        score -= 10;
        isSoft = false;
    }
}

void Score::Reset()
{
    score = 0;
    isSoft = false;
}
