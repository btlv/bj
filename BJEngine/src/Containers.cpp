#include "Containers.h"

void GameActionsTable::Serialize(std::ostream& os) const
{
    const size_t tableSize = size();
    os.write((const char*)&tableSize, sizeof(tableSize));
    for(GameActionsTable::const_iterator it = begin(), en = end(); it != en; ++it)
        os.write((const char*)&(*it), sizeof(GameActionProfit));
}

void GameActionsTable::UnSerialize(std::istream& is)
{
    size_t tableSize = 0;
    is.read((char*)&tableSize, sizeof(tableSize));
    erase(begin(), end());
    GameActionProfit gaProf;
    for(size_t i = 0; i < tableSize; ++i)
    {
        is.read((char*)&gaProf, sizeof(GameActionProfit));
        insert(gaProf);
    }
}

void GameActionsLib::Serialize(std::ostream& os) const
{
    const size_t libSize = size();
    os.write((const char*)&libSize, sizeof(libSize));
    for(GameActionsLib::const_iterator it = begin(), en = end(); it != en; ++it)
    {
        it->first.Serialize(os);
        it->second.Serialize(os);
    }
}

void GameActionsLib::UnSerialize(std::istream& is)
{
    size_t libSize = 0;
    is.read((char*)&libSize, sizeof(libSize));
    erase(begin(), end());
    SituationDesc desc;
    GameActionsTable table;
    for(size_t i = 0; i < libSize; ++i)
    {
        desc.UnSerialize(is);
        table.UnSerialize(is);
        insert(std::make_pair(desc, table));
    }
}
