#include "Rules.h"

bool Rules::IsAllowed(const Box& box, size_t handNum, GameAction::GameAction decision) const
{
    const bool boxWasPairOfAces = box.SplitsDone() && box.WasPairOfAces();
    switch(decision)
    {
    case(GameAction::GAHit):
        {
            if(boxWasPairOfAces && box.GetHand(handNum).GetCardsCount() > 1)
                return false;
            return true;
        }
    case(GameAction::GADouble):
        {
            if(boxWasPairOfAces)
                return false;
            if(2 == box.GetHand(handNum).GetCardsCount() && (box.HandsCount() == 1 || dblAfterSplitAllowed))
            {
                if(dbl == DAny)
                    return true;
                else
                {
                    const unsigned int score = box.GetHand(handNum).GetScore().score;
                    if(dbl == DOnly_10_11 && (score == 10 || score == 11))
                        return true;
                    else if(dbl == DOnly_9_10_11 && score >= 9 && score <= 11)
                        return true;
                    return false;
                }
            }
            return false;
        }
    case(GameAction::GASplit):
        {
            if(box.HandsCount() == 1 && maxSplits)
                return true;
            if(box.SplitsDone() < maxSplits)
            {
                if(box.GetHand(handNum).IsAllAces() && !acesResplitAllowed)
                    return false;
                return true;
            }
            return false;
        }
    case(GameAction::GASurrender):
        {
            if(box.HandsCount() == 1 && box.GetHand(0).GetCardsCount() == 2 && surr != SNo)
                return true;
            return false;
        }
    default:
        return true;
    }
}

Rules::Rules():
        dbl(DAny)
        ,maxSplits(3)
        ,surr(SNo)
        ,dealerHitsS17(false)
        ,dealerPeeks(false)
        ,dblAfterSplitAllowed(true)
        ,acesResplitAllowed(false)
{
}

void Rules::Serialize(std::ostream& os) const
{
    os.write((const char*)this, sizeof(Rules));
}

void Rules::UnSerialize(std::istream& is)
{
    is.read((char*)this, sizeof(Rules));
}
