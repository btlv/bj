#include "StrategyTranslator.h"
#include <assert.h>
#include <sstream>

static const size_t rowCount = 32;
static const size_t colCount = 10;

static Card RowToHand(size_t row, size_t firstOrSecond)
{
	static const Card rowToHand[rowCount][2] =
	{
		{Card3, Card2}
		, {Card4, Card2}
		, {Card5, Card2}
		, {Card6, Card2}
		, {Card7, Card2}
		, {Card8, Card2}
		, {Card9, Card2}
		, {Card10, Card2}
		, {Card10, Card3}
		, {Card10, Card4}
		, {Card10, Card5}
		, {Card10, Card6}
		, {Card10, Card7}
		, {Card10, Card8}
		, {CardA, Card2}
		, {CardA, Card3}
		, {CardA, Card4}
		, {CardA, Card5}
		, {CardA, Card6}
		, {CardA, Card7}
		, {CardA, Card8}
		, {CardA, Card9}
		, {Card2, Card2}
		, {Card3, Card3}
		, {Card4, Card4}
		, {Card5, Card5}
		, {Card6, Card6}
		, {Card7, Card7}
		, {Card8, Card8}
		, {Card9, Card9}
		, {Card10, Card10}
		, {CardA, CardA}
	};
	assert(row < 32);
	assert(firstOrSecond == 0 || firstOrSecond == 1);
	return rowToHand[row][firstOrSecond];
}

static Card ColumnToDlrCard(size_t col)
{
	static const Card columnToDlrCard[] =
	{
		Card2, Card3, Card4, Card5, Card6, Card7, Card8, Card9, Card10, CardA
	};
	assert(col < 10);
	return columnToDlrCard[col];
}

void StrategyTranslatorBase::translate()
{
	SituationDesc desc;
	assert(!lib.empty());
	for(size_t i = 0; i < colCount; ++i)
	{
		for(size_t j = 0; j < rowCount; ++j)
		{
			desc.Set(RowToHand(j, 0), RowToHand(j, 1), ColumnToDlrCard(i));
			GameActionsLib::const_iterator found = lib_.find(desc);
			assert(found != lib.end());
			assert(!found->second.empty());
			translateCell(found->second);
		}
	}
}

void StrategyTranslatorSimple::translate()
{
	res.clear();
	res.reserve(colCount * rowCount);
	StrategyTranslatorBase::translate();
}

void StrategyTranslatorSimple::translateCell(const GameActionsTable& decisions)
{
	const GameAction::GameAction decision = decisions.begin()->action;
	res.append(GameActionToStrShrt(decision));
}

void StrategyTranslator::translateCell(const GameActionsTable& decisions)
{
	std::ostringstream details;
	details.precision(3);
	for(GameActionsTable::const_iterator i = decisions.begin(), end = decisions.end(); i != end; ++i)
	{
		details << GameActionToStrShrt(i->action) << '=';
		const float bjUnknown = i->GetBJUnknownProfit();
		const float noBJ = i->GetNoBJProfit(true);
		details << bjUnknown;
		if(noBJ && noBJ != bjUnknown)
			details << '/' << noBJ;
		details << '\n';
	}
	res.push_back(details.str());
}

void StrategyTranslator::translate()
{
	res.clear();
	res.reserve(colCount * rowCount);
	StrategyTranslatorBase::translate();
}
