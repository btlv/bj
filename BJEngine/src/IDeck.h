#ifndef _I_DECK_H_
#define _I_DECK_H_

#include "Card.h"

class IDeck
{
public:
    virtual Card GetCard() = 0;
    virtual ~IDeck() {}
};

#endif

