#pragma once
#include "stdafx.h"
#include <boost/shared_ptr.hpp>

class GameActionsLib;
struct Rules;

class BJ_EXPORT IStrategyCalculator
{
public:
	virtual bool BuildNextPiece() = 0;//returns false if building is finished

	virtual float BuiltPart() const = 0;

	virtual bool IsFinished() const = 0;

	virtual const GameActionsLib& GetGameActionsLib() const = 0;

	virtual void Reset(const Rules& rules, size_t maxThreads = 10) = 0;

	virtual void Set(const GameActionsLib& lib, const Rules& rules) = 0;

	virtual ~IStrategyCalculator(void) {}
};

typedef boost::shared_ptr<IStrategyCalculator> IStrategyCalculatorPtr;
