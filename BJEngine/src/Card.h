#ifndef _CARD_H_
#define _CARD_H_

#include <ostream>

enum Card
{
    CardFirst = 1,
    Card2,
    Card3,
    Card4,
    Card5,
    Card6,
    Card7,
    Card8,
    Card9,
    Card10,
    CardJ,
    CardQ,
    CardK,
    CardA,
    CardLast
};

struct Score
{
    inline static unsigned int CardsScores(Card card)
    {
        static const unsigned int cardsScores[] =
        {
            0, 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11
        };
        return cardsScores[card];
    };

    inline static Card Score2Card(unsigned int scr)
    {
        static const Card int2Card[] =
        {
            CardLast,
            CardA,
            Card2,
            Card3,
            Card4,
            Card5,
            Card6,
            Card7,
            Card8,
            Card9,
            Card10,
            CardA
        };
        return int2Card[scr];
    }

    Score()
    {
        Reset();
    }

    unsigned int score;
    bool         isSoft;

    void AddCard(Card card);
    void Reset();

    friend std::ostream& operator <<(std::ostream& os, const Score& score)
    {
        if(score.isSoft)
            os << "soft ";
        else
            os << "hard ";
        os << score.score;
        return os;
    };
};


#endif
