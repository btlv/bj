#ifndef _INFINITE_DECK_H_
#define _INFINITE_DECK_H_

#include "IDeck.h"

class InfiniteDeck: public IDeck
{
private:
    const float randMult_;
public:
    virtual Card GetCard();

    InfiniteDeck();
    ~InfiniteDeck() {};
};

#endif
