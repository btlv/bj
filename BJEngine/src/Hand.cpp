#include "Hand.h"

#include <assert.h>

#include <iostream>

Hand::Hand()
{
    Init();
}

Hand::Hand(Card card)
{
    Init();
    AddCard(card);
}

Hand::Hand(Card card1, Card card2)
{
    Init();
    AddCard(card1);
    AddCard(card2);
    if(GetScore().score == 21)
    {
        isBJ_ = true;
        isLocked_ = true;
    }
}

void Hand::Init()
{
    isLocked_ = false;
    isBJ_ = false;
    forcedUnPair_ = false;
    cards_.reserve(6);
}

void Hand::Reset()
{
    cards_.erase(cards_.begin(), cards_.end());
    score_.Reset();
    Init();
}


const Score& Hand::GetScore() const
{
    return score_;
}

void Hand::AddCard(Card card)
{
    assert(!isLocked_);
    if(!isLocked_)
    {
        //static const char* card2String[] =
        //{
        //	"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"
        //};
        //std::cout << card2String[card] << ' ';

        cards_.push_back(card);
        score_.AddCard(card);
    }
}

void Hand::CompleteHand(IDeck& deck, unsigned int hardScore, unsigned int softScore)
{

    while((score_.score * !score_.isSoft) < hardScore && (score_.score * score_.isSoft) < softScore)
        AddCard(deck.GetCard());
}

void Hand::ForceUnPair()
{
    forcedUnPair_ = true;
}

Card Hand::IsPair() const
{
    if(cards_.size() != 2 || cards_[0] != cards_[1] || forcedUnPair_)
        return CardLast;
    return cards_[0];
}

Card Hand::Split()
{
    assert(IsPair() != CardLast);
    cards_.pop_back();
    score_.Reset();
    Card theCard = cards_[0];
    score_.AddCard(theCard);
    return theCard;
}

bool Hand::IsBJ() const
{
    return isBJ_;
}

bool Hand::IsAllAces() const
{
    for(size_t i = 0, size = cards_.size(); i < size; ++i)
        if(CardA != cards_[i])
            return false;
    return true;
}

void Hand::Serialize(std::ostream& os) const
{
    const size_t cardsCount = cards_.size();
    os.write((const char*)&cardsCount, sizeof(cardsCount));
    if(cardsCount)
        os.write((const char*)&(cards_[0]), cardsCount * sizeof(Card));
    os.write((const char*)&score_, sizeof(score_));
    os.write((const char*)&isLocked_, sizeof(isLocked_));
    os.write((const char*)&isBJ_, sizeof(isBJ_));
    os.write((const char*)&forcedUnPair_, sizeof(forcedUnPair_));
}

void Hand::UnSerialize(std::istream& is)
{
    size_t cardsCount = 0;
    is.read((char*)&cardsCount, sizeof(cardsCount));
    cards_.resize(cardsCount);
    if(cardsCount)
        is.read((char*)&(cards_[0]), cardsCount * sizeof(Card));
    is.read((char*)&score_, sizeof(score_));
    is.read((char*)&isLocked_, sizeof(isLocked_));
    is.read((char*)&isBJ_, sizeof(isBJ_));
    is.read((char*)&forcedUnPair_, sizeof(forcedUnPair_));
}

PlaingHand::PlaingHand(float bet /* = 1.0f */):
        bet_(bet)
        , isSurrended_(false)
{
}

PlaingHand::PlaingHand(Card card, float bet /* = 1.0f */):
        Hand(card)
        , bet_(bet)
        , isSurrended_(false)
{
}

PlaingHand::PlaingHand(Card card1, Card card2, float bet /* = 1.0f*/):
        Hand(card1, card2)
        , bet_(bet)
        , isSurrended_(false)
{
}

PlaingHand::PlaingHand(const Hand& hand, float bet /* = 1.0f */):
        Hand(hand)
        , bet_(bet)
        , isSurrended_(false)
{
}

void PlaingHand::Surrender()
{
    isSurrended_ = true;
    Lock();
}

void PlaingHand::Double(Card card)
{
    bet_ *= 2.f;
    AddCard(card);
    Lock();
}

PlaingHand PlaingHand::Split()
{
    return PlaingHand(Hand::Split(), bet_);
}

bool PlaingHand::IsBusted() const
{
    return GetScore().score > 21;
}

float PlaingHand::TakeProfit(const Hand& dealerHand) const
{
    const unsigned int score = GetScore().score;
    assert(!isSurrended_ || (isSurrended_ && score <= 21));

    if(score > 21)
        return -bet_;

    if(isSurrended_)
        return -0.5f * bet_;

    if(IsBJ() && !dealerHand.IsBJ())
        return 1.5f * bet_;

    const unsigned int dealerScore = dealerHand.GetScore().score;
    if(dealerScore > 21 || score > dealerScore)
        return bet_;
    if(score < dealerScore || (dealerHand.IsBJ() && !IsBJ()))
        return -bet_;
    return 0.f;
}
