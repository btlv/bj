#ifndef _CORE_H_
#define _CORE_H_

#include "stdafx.h"
#include <fstream>
#include "IStrategyCalculator.h"

class IDeck;

class BJ_EXPORT Core
{
private:
    IDeck*								deck_;
	std::ofstream						log_;

    Core();
    Core(const Core& core);
    Core& operator = (const Core& core) {return *this;}
    ~Core();

public:
    IDeck& GetDeck();
	std::ofstream& GetLog();

	IStrategyCalculatorPtr NewStrategyCalculator();

	static Core& GetCore();
};

#endif
