#ifndef _ACTION_H_
#define _ACTION_H_

#include "Hand.h"
#include "stdafx.h"

namespace GameAction
{
    enum GameAction
    {
        GAStand = 0,
        GAHit,
        GADouble,
        GASplit,
        GASurrender,
        GALast
    };

    inline const char* GameActionToStr(GameAction decision)
    {
        static const char* decision2Str[] =
        {
            "Stand",
            "Hit",
            "Double",
            "Split",
            "Surrender"
        };
        return decision2Str[decision];
    }

    inline const char* GameActionToStrShrt(GameAction decision)
    {
        static const char* decision2StrShrt[] =
        {
            "S",
            "H",
            "D",
            "P",
            "R"
        };
        return decision2StrShrt[decision];
    }

	inline GameAction CharToGameAction(char c)
	{
		if(c > 'A' && c < 'Z')
			c += 'a' - 'A';
		switch(c)
		{
		case 's': return GAStand;
		case 'h': return GAHit;
		case 'd': return GADouble;
		case 'p': return GASplit;
		case 'r': return GASurrender;
		}
		return GALast;
	}
}

class SituationDesc: public Hand
{
private:
    unsigned int dlrCardScore_;

public:
    BJ_EXPORT SituationDesc();
    BJ_EXPORT SituationDesc(Card plCard1, Card plCard2, Card dlrUpCard);
    BJ_EXPORT SituationDesc(const Hand& hand, unsigned int dlrCardScore);
    BJ_EXPORT void Set(Card plCard1, Card plCard2, Card dlrUpCard);
    BJ_EXPORT void Set(Card plCard, Card dlrUpCard);
    unsigned int GetDlrScore() const {return dlrCardScore_;}

    bool operator < (const SituationDesc& other) const
    {
        const Score& score = GetScore();
        const Score& otherScore = other.GetScore();
        if(score.isSoft < otherScore.isSoft)
            return true;
        else if(score.isSoft == otherScore.isSoft)
        {
            if(dlrCardScore_ < other.dlrCardScore_)
                return true;
            else if(dlrCardScore_ == other.dlrCardScore_)
            {
                if(score.score < otherScore.score)
                    return true;
                else if(score.score == otherScore.score)
                {
                    if(IsPair() != CardLast && other.IsPair() == CardLast)
                        return true;
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    void Serialize(std::ostream& os) const;
    void UnSerialize(std::istream& is);
};

struct AllSituations 
{
    static const size_t SituationsCount = 380;
    static const size_t IndependentSituationsLayerCount = 10;
    SituationDesc situations[SituationsCount];

    AllSituations();
};

class BJ_EXPORT GameActionProfit
{
public:
    GameAction::GameAction  action;
private:
    double                  profitBJUnknown_;
    double                  profitNoBJ_;

    unsigned long           bjUnknownTimes_;
    unsigned long           noBJTimes_;

public:
    GameActionProfit(GameAction::GameAction a);

    GameActionProfit();

    void AddBJUnknownProfit(float prof);

    void AddNoBJProfit(float prof);

    void AddProfit(bool dlrPeeks, float prof);

    void AddingFinished();

    float GetBJUnknownProfit() const;

    float GetNoBJProfit(bool dlrPeeks) const;

    bool operator < (const GameActionProfit& other) const;
};
#endif
