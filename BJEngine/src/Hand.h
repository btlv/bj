#ifndef _HAND_H_
#define _HAND_H_
#include "IDeck.h"
#include "stdafx.h"
#include <vector>
#include <fstream>

class Hand
{
private:
    std::vector<Card> cards_;
    Score             score_;
    bool              isLocked_;
    bool              isBJ_;
    bool              forcedUnPair_;

    void Init();

protected:
    void Lock() {isLocked_ = true;}//no more cards can be added
    Card Split();
    void Reset();

public:
    Hand();
    Hand(Card card);
    Hand(Card card1, Card card2);

    void CompleteHand(IDeck& deck, unsigned int hardScore, unsigned int softScore);
    size_t GetCardsCount() const {return cards_.size();}
    void AddCard(Card card);
    BJ_EXPORT const Score& GetScore() const;
    bool IsBJ() const;
    void ForceUnPair();
    BJ_EXPORT Card IsPair() const;
    bool IsAllAces() const;

    void Serialize(std::ostream& os) const;
    void UnSerialize(std::istream& is);
};

class PlaingHand: public Hand
{
private:
    float bet_;
    bool  isSurrended_;
public:
    PlaingHand(float bet = 1.0f);
    PlaingHand(Card card, float bet = 1.0f);
    PlaingHand(Card card1, Card card2, float bet = 1.0f);
    PlaingHand(const Hand& hand, float bet = 1.0f);

    void Surrender();
    void Double(Card card);
    PlaingHand Split();

    bool IsBusted() const;
    float TakeProfit(const Hand& dealerHand) const;//negative in case of lose
};

#endif
