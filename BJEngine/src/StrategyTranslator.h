#ifndef _STRATEGY_TRANSLATOR_
#define _STRATEGY_TRANSLATOR_
#include <string>
#include <vector>

#include "Containers.h"

class StrategyTranslatorBase
{
protected:
	GameActionsLib lib_;
	virtual void translateCell(const GameActionsTable&) = 0;
public:
	BJ_EXPORT virtual void translate();
	StrategyTranslatorBase(const GameActionsLib& lib):lib_(lib){}
};

class StrategyTranslatorSimple: public StrategyTranslatorBase
{
private:
	std::string res;
protected:
	BJ_EXPORT virtual void translateCell(const GameActionsTable&);
public:
	BJ_EXPORT virtual void translate();
	BJ_EXPORT StrategyTranslatorSimple(const GameActionsLib& lib):StrategyTranslatorBase(lib){}
	BJ_EXPORT const std::string& getTranslation() {return res;}
};

class StrategyTranslator: public StrategyTranslatorBase
{
private:
	std::vector<std::string> res;
protected:
	BJ_EXPORT virtual void translateCell(const GameActionsTable&);
public:
	BJ_EXPORT virtual void translate();
	BJ_EXPORT StrategyTranslator(const GameActionsLib& lib):StrategyTranslatorBase(lib){}
	BJ_EXPORT const std::vector<std::string>& getTranslation() {return res;}
};

#endif