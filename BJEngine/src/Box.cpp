#include "Box.h"
#include "Rules.h"
#include <assert.h>


Box::Box(Card card1, Card card2)
    :splitsDone_(0)
    , initialAcesOnly_(card1 == CardA && card2 == CardA)
{
    hands_.reserve(3);
    hands_.push_back(PlaingHand(card1, card2));
}

Box::Box(const Hand& hand)
    :splitsDone_(0)
    , initialAcesOnly_(hand.IsPair() == CardA)
{
    hands_.reserve(3);
    hands_.push_back(PlaingHand(hand));
}

void Box::Double(size_t i, IDeck& deck)
{
    assert(i < hands_.size());
    hands_[i].Double(deck.GetCard());
}

void Box::Split(size_t i)
{
    assert(i < hands_.size());
    ++splitsDone_;
    hands_.push_back(hands_[i].Split());
}

void Box::SplitUntilPossible(IDeck& deck, const Rules& rules)
{
    assert(hands_.size() == 1 && hands_[0].IsPair() != CardLast);
    size_t pairsCount = 1;
    do
    {
        pairsCount = 0;
        for(size_t i = 0; i < hands_.size(); ++i)
            if(hands_[i].IsPair() != CardLast && rules.IsAllowed(*this, i, GameAction::GASplit))
                Split(i);
        for(size_t i = 0, size = hands_.size(); i < size; ++i)
        {
            if(hands_[i].GetCardsCount() == 1)
                Hit(i, deck);
            if(hands_[i].IsPair() != CardLast && rules.IsAllowed(*this, i, GameAction::GASplit))
                ++pairsCount;
        }
    } while(pairsCount);
}

void Box::Hit(size_t i, IDeck& deck)
{
    assert(i < hands_.size());
    hands_[i].AddCard(deck.GetCard());
}

void Box::CompleteHand(size_t i, IDeck& deck, unsigned int hardScore, unsigned int softScore)
{
    assert(i < hands_.size());
    hands_[i].CompleteHand(deck, hardScore, softScore);
}

void Box::Surrender()
{
    for (size_t i = 0, size = hands_.size(); i < size; ++i)
        hands_[i].Surrender();
}

float Box::TakeProfit(const Hand& dealerHand) const
{
    float profit = 0;
    for(size_t i = 0, size = hands_.size(); i < size; ++i)
        profit += hands_[i].TakeProfit(dealerHand);
    return profit;
}
