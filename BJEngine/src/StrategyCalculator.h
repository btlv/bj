#ifndef _STRATEGY_CALCULATOR_H_
#define _STRATEGY_CALCULATOR_H_

#include "Task.h"
#include "Rules.h"
#include "IStrategyCalculator.h"

#include "ThreadPool\threadpool.hpp"

class StrategyCalculator: public IStrategyCalculator
{
private:
    GameActionsLib          lib_;
    AllSituations           situations_;
    Rules                   rules_;

    boost::mutex            sync_;
    boost::threadpool::pool threadPool_;

    size_t                  finishedCount_;
public:
    StrategyCalculator(const Rules& rules, size_t maxThreads = 10);

    ~StrategyCalculator();

    bool BuildNextPiece();//returns false if building is finished

    float BuiltPart() const {return (float)(finishedCount_)/AllSituations::SituationsCount;}

    bool IsFinished() const {return finishedCount_ == AllSituations::SituationsCount;}

    const GameActionsLib& GetGameActionsLib() const {return lib_;}

    void Reset(const Rules& rules, size_t maxThreads = 10);

    void Set(const GameActionsLib& lib, const Rules& rules);
};

#endif
